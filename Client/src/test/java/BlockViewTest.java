import RISC.Client.BlockView;
import RISC.Client.BlockViewFactory;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Territory;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BlockViewTest {
    public AnchorPane mapAp;
    // assertEquals(10, b1.getWidth());
    // assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20,inBoundsRuleChecker,'X'));
    @Test
    public void test_blockViewFactory() {
        List<String> territories= new ArrayList<>();;
        GameInfo gameInfo =  new GameInfo(true);
        territories.add("Test1");
        territories.add("Test2");
        territories.add("Test3");
        territories.add("Test4");
        for(int i=0;i<9;i++){
            gameInfo.map.getTerritoryByName(territories.get(i)).increaseUnitsNum(0,i);
        }
        for(Territory territory: gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            territories.add(territory.getName());
        }
        BlockViewFactory blockViewFactory = new BlockViewFactory(gameInfo,mapAp);
        assertEquals(400, mapAp.getWidth());
        assertEquals(600, mapAp.getWidth());
        assertEquals(600, mapAp.getWidth());


        //assertThrows(IllegalArgumentException.class, () -> new BlockViewFactory());
    }
}
