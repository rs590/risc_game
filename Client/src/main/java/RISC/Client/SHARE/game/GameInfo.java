package RISC.Client.SHARE.game;

import java.io.Serializable;
import java.util.List;

public class GameInfo implements Serializable {
    public int playerNum; //number of players
    public int initialUnitNum; //initial unit number
    public GameMap map; //game map
    public Player player; //player of receiver
    public List<Player> playerList; //all players
    public boolean isGameEnd; //is game over
    public boolean loginSuccess;
    public boolean isGameAlreadyInitialized;
    public Results results;

    public GameInfo(boolean loginSuccess) {
        this.loginSuccess = loginSuccess;
    }

    public GameInfo(GameMap map, Player player, List<Player> playerList, boolean isGameEnd, int playerNum, int initialUnitNum, boolean isGameAlreadyInitialized) {
        this.playerNum = playerNum;
        this.initialUnitNum = initialUnitNum;
        this.map = map;
        this.player = player;
        this.playerList = playerList;
        this.isGameEnd = isGameEnd;
        this.isGameAlreadyInitialized = isGameAlreadyInitialized;
        this.results = new Results();
    }
}
