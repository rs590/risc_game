package RISC.Client.SHARE.action;


import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.SpyUnit;
import RISC.Client.SHARE.game.Territory;
import RISC.Client.SHARE.result.MoveResult;

import java.util.Map;

/**
 * Spy move, only 1 move in 1 round
 */
public class SpyMoveAction extends Action {

    public SpyMoveAction() {
        super();
        this.ruleChecker = null;
    }

    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        for (int i = 0; i < from.size(); i++) {
            Territory territoryFrom = gameMap.getTerritoryByName(from.get(i));
            Territory territoryTo = gameMap.getTerritoryByName(to.get(i));
            for (SpyUnit spy : player.getSpyUnits()) {
                if (spy.getCurrentTerritory().getName().equals(territoryFrom.getName())) {
                    spy.move(territoryTo);
                    spy.setAvailableToMove(false);
                    break;
                }
            }
            MoveResult moveResult = new MoveResult(territoryFrom,territoryTo,0,1);
            player.results.addAResult(moveResult);
        }
        return null;
    }

    public void addOneAction(String from, String to) {
        this.from.add(from);
        this.to.add(to);
    }
}
