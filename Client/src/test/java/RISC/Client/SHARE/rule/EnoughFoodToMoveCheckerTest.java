package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughFoodToMoveCheckerTest {
    @Test
    public void test_enoughFoodToMove() {
        EnoughFoodToMoveChecker enoughFoodToMoveChecker = new EnoughFoodToMoveChecker(null);
        assertThrows(NullPointerException.class, ()->enoughFoodToMoveChecker.checkMyRule(null, null, null));
    }

}