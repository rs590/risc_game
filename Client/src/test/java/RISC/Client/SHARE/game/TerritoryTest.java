package RISC.Client.SHARE.game;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class TerritoryTest {
    @Test
    public void test_territory() {
        Territory test = new Territory("test", 0);
        test.setOwner(0);
        assertFalse(test.isVisible(1));
        assertTrue(test.isVisible(0));

        Territory test2 = new Territory("test2", 0, 2, 50, 50);
        test.addNeighbor(test2);
        test2.addNeighbor(test);
        assertNotNull(test.getOwnedNeighbors());

        ArrayList<Integer> unitList = new ArrayList<>();
        unitList.add(0); unitList.add(0);
        unitList.add(0); unitList.add(0);
        unitList.add(0); unitList.add(0);
        unitList.add(0);
        test.increaseUnitsNum(unitList);
        test.decreaseUnitsNum(unitList);

        assertNotNull(test.toString());

    }

}