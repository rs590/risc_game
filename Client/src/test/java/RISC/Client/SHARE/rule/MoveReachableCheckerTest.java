package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MoveReachableCheckerTest {
    @Test
    public void test_moveReachable() {
        MoveReachableChecker moveReachableChecker = new MoveReachableChecker(null);
        assertThrows(NullPointerException.class, ()->moveReachableChecker.checkMyRule(null, null, null));
    }

}