package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughUnitsToConsumeCheckerTest {
    @Test
    public void test_enoughUnitsToConsume() {
        EnoughUnitsToConsumeChecker enoughUnitsToConsumeChecker = new EnoughUnitsToConsumeChecker(null);
        assertThrows(NullPointerException.class, ()->enoughUnitsToConsumeChecker.checkMyRule(null, null, null));
    }

}