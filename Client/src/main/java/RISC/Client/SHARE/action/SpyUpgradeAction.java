package RISC.Client.SHARE.action;

import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.result.UpgradeResult;

import java.util.Map;

public class SpyUpgradeAction extends Action {

    //todo rule checker
    public SpyUpgradeAction() {
        super();
        this.ruleChecker = null;
    }

    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        for (int i = 0; i < from.size(); i++) {
            String updateWhere = this.from.get(i);
            int updateUnitNum = this.updateUnitNum.get(i);
            // calculate total tech needed
            int upgradeCost = 20 * updateUnitNum;

            //decrease tech resource
            player.decreaseTotalTechResource(upgradeCost);

            //decrease basic unit
            gameMap.getTerritoryByName(updateWhere).updateUnitList(-updateUnitNum, 0);

            //add new spies
            player.addSpy(updateUnitNum, gameMap.getTerritoryByName(updateWhere));

            UpgradeResult upgradeResult = new UpgradeResult(gameMap.getTerritoryByName(updateWhere),0,7
            ,updateUnitNum,upgradeCost);
            player.results.addAResult(upgradeResult);
        }
        return null;
    }


    public void addOneAction(String from, int num) {
        this.from.add(from);
        this.updateUnitNum.add(num);
    }
}
