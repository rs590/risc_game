package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SameOwnerCheckerTest {
    @Test
    public void sameOwner() {
        SameOwnerChecker sameOwnerChecker = new SameOwnerChecker(null);
        assertThrows(NullPointerException.class, ()->sameOwnerChecker.checkMyRule(null, null, null));
    }
}