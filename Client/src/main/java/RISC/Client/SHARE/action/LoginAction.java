package RISC.Client.SHARE.action;

import java.io.Serializable;

public class LoginAction implements Serializable {
    public int playerID;
    public String password;
    // login action
    public LoginAction(int playerID, String password) {
        this.playerID = playerID;
        this.password = password;
    }
}
