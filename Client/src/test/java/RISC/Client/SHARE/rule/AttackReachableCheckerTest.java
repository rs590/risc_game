package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class AttackReachableCheckerTest {
    @Test
    public void test_attackReachable() {
        AttackReachableChecker attackReachableChecker = new AttackReachableChecker(null);
        assertThrows(NullPointerException.class, ()->attackReachableChecker.checkMyRule(null, null, null));
    }

}