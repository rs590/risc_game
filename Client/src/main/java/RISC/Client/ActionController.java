package RISC.Client;

import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.result.*;
import javafx.animation.FadeTransition;
import javafx.animation.FillTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class ActionController implements Initializable {
    public Label L_name;
    public Label L_maxTech;
    public Label L_tech;
    public Label L_food;
    public Label Warnings;
    public Label watcher;
    public AnchorPane mapAp;
    public Rectangle pcolor;
    public ListView option_list;
    public Button next_turn;
    public ImageView background;
    public Button skip;

    private GameInfo gameInfo;
    private ViewAlter viewAlter;
    private ObservableList<String> actions;
    static List<Color> unit_colors = Arrays.asList(Color.valueOf("#d41eeb"),Color.valueOf("#5a1eeb"),
            Color.valueOf("DODGERBLUE"),Color.valueOf("#1eeb3d"),Color.valueOf("#e8ff37"),Color.valueOf("#ffad1f"),
            Color.valueOf("#ff301f"),Color.BLACK);

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        actions = FXCollections.observableArrayList();
        option_list.setItems(actions);
    }

    private void setButtonType(Button button){
        button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white; -fx-font: 15 arial;");
        button.setEffect(new DropShadow());
        button.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: #FFC125;-fx-font: 18 arial;-fx-border-color: green;");
                button.setEffect(null);


            }
        });
        button.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 15 arial;");
                button.setEffect(new DropShadow());
            }
        });
    }

    public void init(){
        background.setFitWidth(640);
        background.setFitHeight(480);
        L_name.setText(String.valueOf(gameInfo.player.getId()));
        L_tech.setText(String.valueOf(gameInfo.player.getTotalTechResource()));
        L_food.setText(String.valueOf(gameInfo.player.getTotalFoodResource()));
        L_maxTech.setText(String.valueOf(gameInfo.player.getTechLevel()));
        pcolor.setFill(BlockView.colors.get(gameInfo.player.getId()));
        setButtonType(next_turn);
        setButtonType(skip);
    }

    public void setApp(ViewAlter viewAlter) {
        this.viewAlter = viewAlter;
    }

    public void setGamInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public void getNextInfo(){
        if(gameInfo.player.results.isEmpty()){
            next_turn.setVisible(true);
            return;
        }
        mapAp.getChildren().clear();
        if(!gameInfo.player.results.list_upgrade.isEmpty()){
            playUpgradeAnime(gameInfo.player.results.list_upgrade.get(0));
            gameInfo.player.results.list_upgrade.remove(0);
            return;
        }
        if(!gameInfo.player.results.list_move.isEmpty()){
            playMoveAnime(gameInfo.player.results.list_move.get(0));
            gameInfo.player.results.list_move.remove(0);
            return;
        }
        if(!gameInfo.player.results.list_attack.isEmpty()){
            playAttackAnime(gameInfo.player.results.list_attack.get(0));
            gameInfo.player.results.list_attack.remove(0);
            return;
        }
        if(!gameInfo.player.results.list_defend.isEmpty()){
            playDefendAnime(gameInfo.player.results.list_defend.get(0));
            gameInfo.player.results.list_defend.remove(0);
        }
        if(!gameInfo.player.results.list_cloak.isEmpty()){
            playCloakAnime(gameInfo.player.results.list_cloak.get(0));
            gameInfo.player.results.list_cloak.remove(0);
        }
    }

    private void playCloakAnime(CloakResult cloakResult) {
        actions.add("You cloak " + cloakResult.where.getName());
        Rectangle rectangle1 = new Rectangle(150,100,100,50);
        rectangle1.setFill(pcolor.getFill());
        Text text1 = new Text(cloakResult.where.getName());
        text1.setLayoutX(150);
        text1.setLayoutY(90);
        FillTransition ft = new FillTransition();
        ft.setDuration(Duration.seconds(3));
        ft.setShape(rectangle1);
        ft.setToValue(BlockView.colors.get(5));
        mapAp.getChildren().addAll(rectangle1,text1);
        ft.play();
        ft.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getNextInfo();
            }
        });
    }

    public void playMoveAnime(MoveResult moveResult){
        actions.add("You move " + moveResult.num +  " units to " + moveResult.to.getName());
        Rectangle rectangle = new Rectangle(80,125,10,10);
        rectangle.setFill(unit_colors.get(0));
        rectangle.setRotate(45);
        Rectangle rectangle1 = new Rectangle(30,100,100,50);
        rectangle1.setFill(pcolor.getFill());
        Text text1 = new Text(moveResult.from.getName());
        text1.setLayoutX(30);
        text1.setLayoutY(90);
        Rectangle rectangle2 = new Rectangle(250,100,100,50);
        Text text2 = new Text(moveResult.to.getName());
        text2.setLayoutX(250);
        text2.setLayoutY(90);
        rectangle2.setFill(BlockView.colors.get(moveResult.to.getOwnerID()));
        TranslateTransition tt = new TranslateTransition();
        tt.setDuration(Duration.seconds(5));
        tt.setNode(rectangle);
        tt.setFromX(0);
        tt.setToX(200);
        RotateTransition rt = new RotateTransition();
        rt.setDuration(Duration.seconds(3));
        rt.setNode(rectangle);
        rt.setToAngle(360);
        mapAp.getChildren().addAll(rectangle1,rectangle2,rectangle,text1,text2);
        tt.play();
        tt.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                actions.add("cost " + moveResult.foodcost + " food.");
                rt.play();
                rt.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        getNextInfo();
                    }
                });
            }
        });
    }

    public void playAttackAnime(AttackResult attackResult){
        actions.add("You use " + attackResult.num + " level " + attackResult.level + " to attack " + attackResult.to.getName());
        Rectangle rectangle = new Rectangle(80,125,10,10);
        rectangle.setFill(unit_colors.get(attackResult.level));
        rectangle.setRotate(45);
        Rectangle rectangle1 = new Rectangle(30,100,100,50);
        rectangle1.setFill(pcolor.getFill());
        Text text1 = new Text(String.valueOf(gameInfo.player.getId()));
        text1.setLayoutX(30);
        text1.setLayoutY(90);
        Rectangle rectangle2 = new Rectangle(250,100,100,50);
        Text text2 = new Text(attackResult.to.getName());
        text2.setLayoutX(250);
        text2.setLayoutY(90);
        Text text3 = new Text();
        text3.setLayoutX(150);
        text3.setLayoutY(250);
        rectangle2.setFill(BlockView.colors.get(attackResult.owner));
        TranslateTransition tt = new TranslateTransition();
        tt.setDuration(Duration.seconds(4));
        tt.setNode(rectangle);
        tt.setFromX(0);
        tt.setToX(200);
        FillTransition ft = new FillTransition();
        ft.setDuration(Duration.seconds(3));
        ft.setShape(rectangle2);
        ft.setFromValue(BlockView.colors.get(attackResult.owner));
        ft.setToValue(BlockView.colors.get(gameInfo.player.getId()));
        FadeTransition fdt = new FadeTransition();
        fdt.setDuration(Duration.seconds(3));
        fdt.setNode(rectangle);
        fdt.setFromValue(1);
        fdt.setToValue(0);
        mapAp.getChildren().addAll(rectangle1,rectangle2,rectangle,text1,text2,text3);
        tt.play();
        tt.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(attackResult.result){
                    ft.play();
                    text3.setText("You win this battal!");
                    actions.add("You get the territory "+ attackResult.to.getName()+ " and "
                            + attackResult.num_left + " units left.");
                    ft.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            getNextInfo();
                        }
                    });
                } else{
                    fdt.play();
                    text3.setText("You lose this battal!");
                    actions.add("You lost " + attackResult.num + " units.");
                    fdt.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            getNextInfo();
                        }
                    });
                }
            }
        });
    }

    public void playUpgradeAnime(UpgradeResult upgradeResult){
        if(upgradeResult.level_to<7){
            actions.add("Your "+upgradeResult.num+" units upgrade to "+upgradeResult.level_to+" level.");
        } else{
            actions.add("Your "+upgradeResult.num+" units upgrade to spy.");
        }

        Rectangle rectangle = new Rectangle(200,125,10,10);
        rectangle.setFill(unit_colors.get(upgradeResult.level_from));
        rectangle.setRotate(45);
        Rectangle rectangle1 = new Rectangle(150,100,100,50);
        rectangle1.setFill(pcolor.getFill());
        Text text1 = new Text(upgradeResult.where.getName());
        text1.setLayoutX(150);
        text1.setLayoutY(90);
        FadeTransition fdt = new FadeTransition();
        fdt.setDuration(Duration.seconds(0.5));
        fdt.setNode(rectangle);
        fdt.setFromValue(1);
        fdt.setToValue(0);
        fdt.setAutoReverse(true);
        fdt.setCycleCount(6);
        FillTransition ft = new FillTransition();
        ft.setShape(rectangle);
        ft.setToValue(unit_colors.get(upgradeResult.level_to));
        ft.setDuration(Duration.seconds(2));
        mapAp.getChildren().addAll(rectangle1,rectangle,text1);
        fdt.play();
        fdt.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ft.play();
                actions.add("cost " + upgradeResult.techcost + " tech points.");
                ft.setOnFinished(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        getNextInfo();
                    }
                });
            }
        });
    }

    public void playDefendAnime(DefendResult defendResult){
        actions.add("Your territory " + defendResult.to.getName() + " are attacked by " + defendResult.attacker);
        Rectangle rectangle = new Rectangle(280,125,10,10);
        rectangle.setFill(unit_colors.get(defendResult.level));
        rectangle.setRotate(45);
        Rectangle rectangle1 = new Rectangle(30,100,100,50);
        rectangle1.setFill(pcolor.getFill());
        Text text1 = new Text(defendResult.to.getName());
        text1.setLayoutX(30);
        text1.setLayoutY(90);
        Rectangle rectangle2 = new Rectangle(250,100,100,50);
        Text text2 = new Text(String.valueOf(defendResult.attacker));
        text2.setLayoutX(250);
        text2.setLayoutY(90);
        Text text3 = new Text();
        text3.setLayoutX(150);
        text3.setLayoutY(250);
        rectangle2.setFill(BlockView.colors.get(defendResult.attacker));
        TranslateTransition tt = new TranslateTransition();
        tt.setDuration(Duration.seconds(4));
        tt.setNode(rectangle);
        tt.setFromX(0);
        tt.setToX(-200);
        FillTransition ft = new FillTransition();
        ft.setDuration(Duration.seconds(3));
        ft.setShape(rectangle1);
        ft.setToValue(BlockView.colors.get(defendResult.attacker));
        FadeTransition fdt = new FadeTransition();
        fdt.setDuration(Duration.seconds(3));
        fdt.setNode(rectangle);
        fdt.setFromValue(1);
        fdt.setToValue(0);
        mapAp.getChildren().addAll(rectangle1,rectangle2,rectangle,text1,text2,text3);
        tt.play();
        tt.setOnFinished(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(defendResult.result){
                    fdt.play();
                    text3.setText("You defend this attack!");
                    actions.add("You defend "+defendResult.attacker+"'s attack and left " + defendResult.num_left+
                            " units on " + defendResult.to.getName());
                    fdt.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            getNextInfo();
                        }
                    });
                } else{
                    ft.play();
                    text3.setText("You lose " + defendResult.to.getName());
                    actions.add("You lose "+defendResult.to.getName());
                    ft.setOnFinished(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            getNextInfo();
                        }
                    });
                }
            }
        });
    }

    public void skip(ActionEvent actionEvent) throws Exception {
        this.viewAlter.gotoNewGame();
    }

    public void go_to_next_turn(ActionEvent actionEvent) throws Exception {
        this.viewAlter.gotoNewGame();
    }
}
