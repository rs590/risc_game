package RISC.Client.SHARE.action;



import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;
import RISC.Client.SHARE.result.MoveResult;
import RISC.Client.SHARE.rule.*;

import java.util.List;
import java.util.Map;

public class MoveAction extends Action {
    public MoveAction() {
        super();
        this.ruleChecker = new ValidTerritoryNameChecker(new MoveReachableChecker(new SameOwnerChecker(new EnoughUnitsToConsumeMoveChecker(new EnoughFoodToMoveChecker(null)))));
    }

    /**
     * Execute the move action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        String err = ruleChecker.isValid(gameMap, player, this);

        if (err != null) {
            return err;
        }

        for (int i = 0; i < from.size(); i++) {
            Territory territoryFrom = gameMap.getTerritoryByName(from.get(i));
            Territory territoryTo = gameMap.getTerritoryByName(to.get(i));
            int foodCost = gameMap.minimalCostPath(territoryFrom, territoryTo); // find minimal cost path
            int num = 0;
            List<Integer> unitToMoveList = units.get(i);
            for (int level = 0; level < unitToMoveList.size(); ++level) {
                territoryFrom.updateUnitList(-unitToMoveList.get(level), level);
                territoryTo.updateUnitList(unitToMoveList.get(level), level);
                num += unitToMoveList.get(level);
                if (unitToMoveList.get(level) > 0) {
                    foodCost = foodCost * unitToMoveList.get(level);
                }
            }
            //create new move result
            MoveResult moveResult = new MoveResult(territoryFrom, territoryTo, foodCost, num);
            player.results.addAResult(moveResult);

            player.decreaseTotalFoodResource(foodCost); //consume food
        }
        return null;
    }
}

