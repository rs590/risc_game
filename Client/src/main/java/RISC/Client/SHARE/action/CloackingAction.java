package RISC.Client.SHARE.action;


import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;
import RISC.Client.SHARE.result.CloakResult;

import java.util.Map;

public class CloackingAction extends Action {

    public CloackingAction() {
        super();
        this.ruleChecker = null;
    }

    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        for (String terriName : from) {
            Territory from = gameMap.getTerritoryByName(terriName);
            from.remainingCloaking = 3;
            player.decreaseTotalFoodResource(20);
            CloakResult cloakResult = new CloakResult(from);
            player.results.addAResult(cloakResult);
        }
        return null;
    }

    public void addOneAction(String from) {
        this.from.add(from);
    }
}
