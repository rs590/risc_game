package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughTechToUpgradePlayerCheckerTest {
    @Test
    public void test_enoughTechToUpgradePlayer() {
        EnoughTechToUpgradePlayerChecker enoughTechToUpgradePlayerChecker = new EnoughTechToUpgradePlayerChecker(null);
        assertThrows(NullPointerException.class, ()->enoughTechToUpgradePlayerChecker.checkMyRule(null, null, null));
    }

}