package RISC.Client;

import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.action.SetUnitAction;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Territory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Control the set unit view of game*/
public class SetUnitsController implements Initializable {
    public Label L_name;
    public Label L_maxTech;
    public Label L_tech;
    public Label L_food;
    public ListView Options;
    public AnchorPane mapAp;
    public Rectangle pcolor;
    public AnchorPane control_an;
    public Label number;
    public Text waiting;
    private List<BlockView> blocks;
    private ViewAlter viewAlter;
    public boolean ready;
    ObservableList<String> option_list;
    public List<Territory> my_territories;
    private BlockViewFactory blockViewFactory;
    List<Integer> unitToput;
    List<String> territories;
    int index;
    int unit_num;
    int number_territories;
    GameInfo gameInfo;
    public Button butt_exit;
    public ImageView background3;
    @Override
    public void initialize(URL location, ResourceBundle resources) {}

    /**
     * play the warn media when error occurs*/
    public void warn_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/battle1.mp3");
                System.out.println(url);
                System.out.println(url.toExternalForm());
                System.out.println(url.toString());
                Media sound = new Media(url.toString());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.5);
                //mediaPlayer.setAutoPlay(true);
                //mediaPlayer.setStartTime(Duration.seconds(1));
                mediaPlayer.setStopTime(Duration.seconds(0.5));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    /**
     * play the OK media when success*/
    public void OK_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/win.wav");
                Media sound = new Media(url.toExternalForm());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.3);
                //mediaPlayer.setAutoPlay(true);
                //mediaPlayer.setStartTime(Duration.seconds(1));
                mediaPlayer.setStopTime(Duration.seconds(0.3));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    /**
     * set the init value of set Unit*/
    public void init(){
        background3.setFitWidth(640);
        background3.setFitHeight(480);
        butt_exit.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 25 arial;");
        butt_exit.setStyle("-fx-background-color: #7d3d8b; -fx-text-fill: white;-fx-font: 15 arial;");
        butt_exit.setEffect(new DropShadow());
        butt_exit.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butt_exit.setStyle("-fx-background-color: #FFC125;-fx-font:  18 arial;-fx-border-color: red;");
                butt_exit.setEffect(null);
            }
        });
        butt_exit.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butt_exit.setStyle("-fx-background-color: #7d3d8b; -fx-text-fill: white;-fx-font: 15 arial;");
                butt_exit.setEffect(new DropShadow());
            }
        });
        
        L_name.setText(String.valueOf(gameInfo.player.getId()));
        L_food.setText(String.valueOf(0));
        L_tech.setText(String.valueOf(0));
        L_maxTech.setText(String.valueOf(1));
        option_list = FXCollections.observableArrayList();
        pcolor.setFill(BlockView.colors.get(gameInfo.player.getId()));
        my_territories = new ArrayList<>();
        index = 0;
        ready = false;
        unit_num = gameInfo.initialUnitNum;
        my_territories.addAll(gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId()));
        option_list.add("Please set your units: " + unit_num + " units are left");
        number.setText(String.valueOf(unit_num));
        Options.setItems(option_list);
        unitToput = new ArrayList<>();
        territories = new ArrayList<>();
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            territories.add(territory.getName());
        }
        number_territories = gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId()).size();
        setChoices();
    }

    private void setBox(double y,String territory,int i){
        int cur_num = 0;
        Text text = new Text("Units for "+territory);
        text.setLayoutX(15);
        text.setLayoutY(y-15);
        Button button1 = new Button("-5");
        Button button2 = new Button("-1");
        Button button3 = new Button("+1");
        Button button4 = new Button("+5");
        Label label = new Label(String.valueOf(cur_num));
        button1.setLayoutX(25);
        button2.setLayoutX(65);
        label.setLayoutX(105);
        button3.setLayoutX(125);
        button4.setLayoutX(165);
        button1.setLayoutY(y);
        button2.setLayoutY(y);
        label.setLayoutY(y+5);
        label.setId("label"+i);
        button3.setLayoutY(y);
        button4.setLayoutY(y);
        control_an.getChildren().addAll(text,button1,button2,button3,label,button4);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(ready){
                    return;
                }
                int cur_num = Integer.parseInt(label.getText());
                if(cur_num<5){
                    unit_num += cur_num;
                    cur_num = 0;
                } else {
                    cur_num -= 5;
                    unit_num += 5;
                }
                number.setText(String.valueOf(unit_num));
                label.setText(String.valueOf(cur_num));
            }
        });
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(ready){
                    return;
                }
                int cur_num = Integer.parseInt(label.getText());
                if(cur_num<1){
                    unit_num += cur_num;
                    cur_num = 0;
                } else {
                    cur_num -= 1;
                    unit_num += 1;
                }
                number.setText(String.valueOf(unit_num));
                label.setText(String.valueOf(cur_num));
            }
        });
        button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(ready){
                    return;
                }
                int cur_num = Integer.parseInt(label.getText());
                if(unit_num>=1) {
                    cur_num += 1;
                    unit_num -= 1;
                }
                number.setText(String.valueOf(unit_num));
                label.setText(String.valueOf(cur_num));
            }
        });
        button4.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(ready){
                    return;
                }
                int cur_num = Integer.parseInt(label.getText());
                if(unit_num>=5) {
                    cur_num += 5;
                    unit_num -= 5;
                } else {
                    cur_num += unit_num;
                    unit_num = 0;
                }
                number.setText(String.valueOf(unit_num));
                label.setText(String.valueOf(cur_num));
            }
        });
    }

    private void setChoices(){
        double initial_y = 90;
        double offset_y = 60;
        int times = 0;
        for(String territory:territories){
            setBox(initial_y+times*offset_y,territory,times);
            times++;
        }
        Button Ok = new Button("OK");
        Ok.setLayoutX(95);
        Ok.setLayoutY(330);
        control_an.getChildren().add(Ok);
        Ok.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for(int i=0;i<number_territories;i++){
                    Label label = (Label) control_an.lookup("#label"+i);
                    int num = Integer.parseInt(label.getText());
                    unitToput.add(num);
                    option_list.add("You add "+num+" units to "+territories.get(i));
                    gameInfo.map.getTerritoryByName(territories.get(i)).increaseUnitsNum(0,num);
                }
                option_list.add("Set units finish!");
                waiting.setVisible(true);
                ready = true;
                Task<Integer> task = new Task<Integer>() {
                @Override
                protected Integer call() throws Exception {
                    viewAlter.waitForUpdate(createMessage());
                    return 0;
                }

                @Override
                protected void succeeded(){
                    try {
                        OK_sound();
                        viewAlter.gotoNewGame();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            new Thread(task).start();
            }
        });
    }

    /**
     * set the map of game*/
    public void setMap(){
        blockViewFactory = new BlockViewFactory(gameInfo,mapAp);
        blocks = blockViewFactory.createMap();
        for(BlockView blockView:blocks){
            blockView.rectangle.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {
                        blockView.rectangle.setStroke(Color.BLACK);
                        blockView.showTerritoryInfo(viewAlter.getPrimaryStage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            blockView.name.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {
                        blockView.rectangle.setStroke(Color.BLACK);
                        blockView.showTerritoryInfo(viewAlter.getPrimaryStage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * exit game and go to log in interface*/
    public void Exit() throws Exception {
        if(!ready){
            viewAlter.closeGame();
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please wait for other players.");
            alert.showAndWait();
        }
    }

//    public void getNumber() throws Exception {
//        Warnings.setText("");
//        String input = num_text.getText();
//        if(index>gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId()).size()-1){
//            return;
//        }
//        if(!isNumeric(input)){
//            num_text.setText("");
//            Warnings.setText("Invalid input!");
//            return ;
//        }
//        int unit = Integer.parseInt(input);
//        if(unit>unit_num||unit<0){
//            num_text.setText("");
//            Warnings.setText("Invalid input!");
//            return ;
//        }
//        unit_num -= unit;
//        unitToput.add(unit);
//        territories.add(my_territories.get(index).getName());
//        if(index==gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId()).size()-1){
//            option_list.add("You add " + unit + " units to " + my_territories.get(index).getName());
//            option_list.add(unit_num + " units are left.");
//            option_list.add("Set Finish. Waiting for other players...");
//            index++;
//            Task<Integer> task = new Task<Integer>() {
//                @Override
//                protected Integer call() throws Exception {
//                    viewAlter.waitForUpdate(createMessage());
//                    return 0;
//                }
//
//                @Override
//                protected void succeeded(){
//                    try {
//                        viewAlter.gotoNewGame();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            new Thread(task).start();
//        } else{
//            option_list.add("You add " + String.valueOf(unit) + " units to " + my_territories.get(index).getName());
//            option_list.add(unit_num + " units are left.");
//            num_text.setText("");
//            index++;
//            ter.setText(my_territories.get(index).getName());
//        }
//    }

    /**
     * Used to judge if the input string is a number.
     * @param str the string needs to be judged.
     * @return true if string is a number, else false.*/
    public static boolean isNumeric(String str) {
        String regEx = "^[0-9]+$";
        Pattern pat = Pattern.compile(regEx);
        Matcher mat = pat.matcher(str);
        return mat.find();
    }

    /**
     * set view alter*/
    public void setApp(ViewAlter viewAlter) {
        this.viewAlter = viewAlter;
    }

    /**
     * set game info*/
    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    /**
     * create message to set units*/
    public Message createMessage() {
        List<List<Integer>> list = new ArrayList<>();
        for(int i:unitToput){
            list.add(new ArrayList<>(Arrays.asList(i,0,0,0,0,0,0)));
        }
        return new Message(new SetUnitAction(territories,list));
    }
}
