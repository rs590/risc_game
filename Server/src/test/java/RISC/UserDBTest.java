package RISC;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserDBTest {
    @Test
    public void test_db() {
        UserDB db = new UserDB();
        assertEquals(true,db.isValid(0,"000"));
        assertEquals(true,db.isValid(1,"111"));
        assertEquals(true,db.isValid(2,"222"));
        assertEquals(true,db.isValid(3,"333"));
        assertEquals(true,db.isValid(4,"444"));
        assertEquals(true,db.isValid(5,"555"));
        assertEquals(false,db.isValid(5,"333"));
        assertEquals(false,db.isValid(5,"5555"));
        assertEquals(false,db.isValid(6,"333"));
        assertEquals(false,db.isValid(51,""));

    }

}