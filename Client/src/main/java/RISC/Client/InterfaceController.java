package RISC.Client;

import RISC.Client.SHARE.action.ChooseGameAction;
import RISC.Client.SHARE.action.ChooseGameRoomInfo;
import RISC.Client.SHARE.action.LoginAction;
import RISC.Client.SHARE.action.Message;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This Interface Controller is used to control the game choice and log in view of game*/
public class InterfaceController implements Initializable {
    public Label notifier;
    public Label room0_state;
    public Label room1_state;
    public Label room2_state;
    public Label room3_state;
    public Label player_msg;
    public Label password_msg;
    private List<Label> rooms;
    @FXML
    private Label errMsg;
    @FXML
    private PasswordField password;
    @FXML
    private TextField player_id;
    boolean flag = true;
    private ViewAlter viewAlter;

    public Button loginButton;
    public Button G1;
    public Button G2;
    public Button G3;
    public Button G4;
    public ImageView background1;
    public ImageView background2;


    protected void showErrorMsg() {
        errMsg.setText("Wrong password or user!");
    }

    /**
     * play the warn media when error occurs*/
    public void warn_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/battle1.mp3");
                System.out.println(url);
                System.out.println(url.toExternalForm());
                System.out.println(url.toString());
                Media sound = new Media(url.toString());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.5);
                mediaPlayer.setStopTime(Duration.seconds(0.5));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    /**
     * play the OK media when success*/
    public void OK_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/win.wav");
                Media sound = new Media(url.toExternalForm());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.3);
                mediaPlayer.setStopTime(Duration.seconds(0.3));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }
    /**
     * do the log in action*/
    @FXML
    public void login_control() {
        String id = player_id.getText();
        player_id.setText("");
        String pwd = password.getText();
        password.setText("");
        System.out.println(id);
        System.out.println(pwd);

        if(!isNumeric(id)){
            warn_sound();
            showErrorMsg();
            return;
        }
        int playerID = Integer.parseInt(id);
        LoginAction loginAction = new LoginAction(playerID, pwd);
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                return viewAlter.waitForLogIn(new Message(loginAction));
            }
            @Override
            protected void succeeded() {
                try {
                    if(this.getValue()==1){
                        warn_sound();
                        showErrorMsg();
                    } else{
                        OK_sound();
                        viewAlter.gotoChoiceGame();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * choose the first game*/
    @FXML
    public void enter_game1(){
        if(flag) {
            OK_sound();
            chooseGame(0);
        };
    }

    /**
     * choose the second game*/
    @FXML
    public void enter_game2(){
        if(flag) {
            OK_sound();
            chooseGame(1);
        };
    }

    /**
     * choose the third game*/
    @FXML
    public void enter_game3(){
        if(flag) {
            OK_sound();
            chooseGame(2);
        };

    }

    /**
     * choose the fourth game*/
    @FXML
    public void enter_game4(){
        if(flag) {
            OK_sound();
            chooseGame(3);
        };
    }

    private void setButtonType(Button button){
        button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 15 arial;");
        button.setEffect(new DropShadow());
        button.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: #FFC125;-fx-font:  18 arial;-fx-border-color: green");
                button.setEffect(null);


            }
        });
        button.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 15 arial;");
                button.setEffect(new DropShadow());
            }
        });
    }

    /**
     * Set up the view alter model*/
    public void setApp(ViewAlter viewAlter) {
        background2.setFitHeight(480);
        background2.setFitWidth(640);
        setButtonType(G1);
        setButtonType(G2);
        setButtonType(G3);
        setButtonType(G4);
        this.viewAlter = viewAlter;
    }
    public void setLogin(ViewAlter viewAlter) {
        player_msg.setStyle(" -fx-text-fill: linear-gradient(from 0% 0% to 100% 200%, repeat, darkgreen 0%, red 50%);");
        player_msg.setFont(Font.font("Verdana", FontPosture.ITALIC, 19));
        password_msg.setStyle(" -fx-text-fill: linear-gradient(from 0% 0% to 100% 200%, repeat, darkgreen 0%, red 50%);");
        password_msg.setFont(Font.font("Verdana", FontPosture.ITALIC, 19));
        background1.setFitWidth(640);
        background1.setFitHeight(480);
        setButtonType(loginButton);
        this.viewAlter = viewAlter;
    }

    /**
     * Used to judge if the input string is a number.
     * @param str the string needs to be judged.
     * @return true if string is a number, else false.*/
    private static boolean isNumeric(String str) {
        String regEx = "^[0-9]+$";
        Pattern pat = Pattern.compile(regEx);
        Matcher mat = pat.matcher(str);
        return mat.find();
    }

    /**
     * The game choose action*/
    private void chooseGame(int choose){
        ChooseGameAction chooseGameAction = new ChooseGameAction(choose);
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                return viewAlter.waitForChooseGame(new Message(chooseGameAction));
            }
            @Override
            protected void succeeded() {
                try {
                    if(this.getValue()==1){
                        notifier.setText("Sorry this game is started! Try another one please.");
                    } else{
                        notifier.setText("Add game successfully. Waiting for other player joining...");
                        flag = false;
                        viewAlter.gotoWaitInterface();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }
}
