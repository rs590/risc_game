package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class SpyUpgradeActionTest {
    @Test
    public void test_spyUpgrade() {
        SpyUpgradeAction spyUpgradeAction = new SpyUpgradeAction();
        assertDoesNotThrow(()->spyUpgradeAction.execute(null, null, null));
        spyUpgradeAction.addOneAction(null, 0);
    }

}