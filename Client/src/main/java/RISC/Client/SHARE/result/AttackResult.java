package RISC.Client.SHARE.result;

import RISC.Client.SHARE.game.Territory;

import java.io.Serializable;

public class AttackResult implements Serializable {
    public Territory to;
    public int owner;
    public int foodcost;
    public int level;
    public int num;
    public int num_left;
    public boolean result;


    public AttackResult(Territory to, int owner, int foodcost, int level, int num, int num_left, boolean result) {
        this.to = to;
        this.owner = owner;
        this.foodcost = foodcost;
        this.level = level;
        this.num = num;
        this.num_left = num_left;
        this.result = result;
    }
}
