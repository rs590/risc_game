package RISC.Client.SHARE.game;

import java.io.Serializable;
import java.util.*;

// Maintainer: Ruitong Su
public class GameMap implements AbstractMap, Serializable {
    // private GameMapFactory factory;
    private LinkedHashSet<Territory> territories;

    // Constructor for testing
    public GameMap() {
        territories = new LinkedHashSet<>();
    }

    @Override
    public Territory getTerritoryByName(String name) {
        for (Territory territory : territories) {
            if (territory.getName().equals(name)) {
                return territory;
            }
        }
        return null;
    }

    public void decreaseCloack() {
        for (Territory territory : territories) {
            if (territory.remainingCloaking > 0) {
                territory.remainingCloaking--;
            }
        }
    }

    public HashSet<Territory> getTerritories() {
        return territories;
    }

    public List<Territory> getTerritoriesList() {
        return new ArrayList<>(territories);
    }

    @Override
    public LinkedHashSet<Territory> getTerritoriesForPlayer(int playerID) {
        LinkedHashSet<Territory> res = new LinkedHashSet<>();
        for (Territory territory : territories) {
            if (territory.getOwnerID() == playerID) {
                res.add(territory);
            }
        }
        return res;
    }

    /**
     * Add one basic unit to all territories
     */
    public void addOneUnitToAllTerritorries() {
        for (Territory territory : territories) {
            territory.updateUnitList(1, 0);
        }
    }

    /**
     * Whether there is a path between two territory for attacking
     *
     * @param from source territory
     * @param to   dest territory
     * @return is reachable
     */
    public boolean isReachableForAttack(Territory from, Territory to) {
        if (from.getOwnerID() == to.getOwnerID()) {
            return false;
        }

        for (Territory neighbor : from.getNeighbors()) {
            if (neighbor.equals(to)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Whether there is a path between two territory for moving
     *
     * @param from source territory
     * @param to   dest territory
     * @return is reachable
     */
    public boolean isReachableForMove(Territory from, Territory to) {
        HashSet<Territory> visited = new HashSet<>();
        visited.add(from);
        return isReachableHelper(from, to, visited);
    }

    /**
     * DFS helper
     *
     * @param from    source territory
     * @param to      dest territory
     * @param visited visited
     * @return is reachable
     */
    private boolean isReachableHelper(Territory from, Territory to, HashSet<Territory> visited) {
        if (from.equals(to)) {
            return true;
        }

        for (Territory neighbor : from.getNeighbors()) {
            if (visited.contains(neighbor) || from.getOwnerID() != neighbor.getOwnerID()) {
                continue;
            }
            visited.add(neighbor);
            if (isReachableHelper(neighbor, to, visited)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Find the minimal cost path
     *
     * @param from source territory
     * @param to   dest territory
     * @return the minimal cost between two territory
     */
    public int minimalCostPath(Territory from, Territory to) {
        Set<Territory> visited = new HashSet<>();
        visited.add(from);

        return minimalCosthelper(from, to, 0, visited);
    }

    // DFS helper function for minimalCostPath
    private int minimalCosthelper(Territory from, Territory to, int currentCost, Set<Territory> visited) {
        currentCost += from.getSize();
        if (from.getName().equals(to.getName())) {
            return currentCost;
        }

        Set<Territory> neighbors = from.getOwnedNeighbors();
        int minCost = Integer.MAX_VALUE;
        for (Territory neighbor : neighbors) {
            if (!visited.contains(neighbor)) {
                visited.add(neighbor);
                minCost = Math.min(minCost, minimalCosthelper(neighbor, to, currentCost, visited));
                visited.remove(neighbor);
            }
        }
        return minCost;
    }

    public void setTerritories(LinkedHashSet<Territory> territories) {
        this.territories = territories;
    }
}
