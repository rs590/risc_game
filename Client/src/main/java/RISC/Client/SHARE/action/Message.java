package RISC.Client.SHARE.action;

import java.io.Serializable;

public class Message implements Serializable {
    public MoveAction moveAction;
    public AttackAction attackAction;
    public SetUnitAction setUnitAction;
    public UpgradeUnitsAction upgradeUnitsAction;
    public UpgradeTechLevelAction upgradeTechLevelAction;
    public LoginAction loginAction;
    public ChooseGameAction chooseGameAction;
    public SpyUpgradeAction spyUpgradeAction;
    public SpyMoveAction spyMoveAction;
    public CloackingAction cloackingAction;

    public Message() {
    }

    public Message(ChooseGameAction chooseGameAction) {
        this.chooseGameAction = chooseGameAction;
        moveAction = null;
        attackAction = null;
        setUnitAction = null;
        loginAction = null;
    }

    public Message(LoginAction loginAction) {
        this.loginAction = loginAction;
        moveAction = null;
        attackAction = null;
        setUnitAction = null;
    }

    public Message(SetUnitAction setUnitAction) {
        this.setUnitAction = setUnitAction;
        moveAction = null;
        attackAction = null;
        loginAction = null;
    }

    public void updateActionList(AttackAction attackAction, MoveAction moveAction, UpgradeUnitsAction upgradeUnitsAction, UpgradeTechLevelAction upgradeTechLevelAction, SpyUpgradeAction spyUpgradeAction, SpyMoveAction spyMoveAction, CloackingAction cloackingAction) {
        if (setUnitAction != null) {
            setUnitAction = null;
        }
        this.attackAction = attackAction;
        this.moveAction = moveAction;
        this.upgradeUnitsAction = upgradeUnitsAction;
        this.upgradeTechLevelAction = upgradeTechLevelAction;
        this.spyUpgradeAction = spyUpgradeAction;
        this.spyMoveAction = spyMoveAction;
        this.cloackingAction = cloackingAction;
    }
}
