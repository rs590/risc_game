package RISC;

import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.utility.SerializeUtility;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is used for sending information to the players
 *
 * @author cirun zhang
 */
public class MessageSender {
    private Game parentGame;
    private SerializeUtility utility; //utility for send and receive

    public MessageSender(Game parentGame) {
        this.parentGame = parentGame;
        this.utility = new SerializeUtility();
    }


    /**
     * Send game info object to all players
     */
    public boolean sendGameInfoToAllPlayers() {
        GameMap map = parentGame.gameMap;
        List<Player> players = new ArrayList<>(parentGame.idToPlayers.values());
        boolean isGameEnd = parentGame.isGameEnd;
        int initialUnitNum = 30;
        boolean isGameAlreadyInitialized = parentGame.isGameAlreadyInitialized;

        for (Map.Entry<Player, Socket> entry : parentGame.playersToSockets.entrySet()) {
            Player player = entry.getKey();
            Socket socket = entry.getValue();
            GameInfo info = new GameInfo(map, player, players, isGameEnd, players.size(), initialUnitNum, isGameAlreadyInitialized);

            try {
                if (!utility.sendGameInfo(info, socket.getOutputStream())) {
                    System.out.println("sending failed");
                    return false; //sending gameinfo failed, the client has disconnected
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Send game info object to a player
     */
    public boolean sendGameInfoToPlayer(Player player) {
        GameMap map = parentGame.gameMap;
        List<Player> players = new ArrayList<>(parentGame.idToPlayers.values());
        boolean isGameEnd = parentGame.isGameEnd;
        int initialUnitNum = 30;
        boolean isGameAlreadyInitialized = parentGame.isGameAlreadyInitialized;
        Socket socket = parentGame.playersToSockets.get(player);
        GameInfo info = new GameInfo(map, player, players, isGameEnd, players.size(), initialUnitNum, isGameAlreadyInitialized);
        try {
            if (!utility.sendGameInfo(info, socket.getOutputStream())) {
                return false; //sending gameinfo failed, the client has disconnected
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
