package RISC.Client;

import RISC.Client.SHARE.action.*;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.SpyUnit;
import RISC.Client.SHARE.game.Territory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class NewGameController implements Initializable {
    public Label L_name;
    public Label L_maxTech;
    public Label L_tech;
    public Label L_food;
    public Label Warnings;
    public ListView Options;
    public ChoiceBox M_from;
    public ChoiceBox M_to;
    public ChoiceBox M_tu;
    public ChoiceBox M_number;
    public ChoiceBox A_tu;
    public ChoiceBox A_from;
    public ChoiceBox A_to;
    public ChoiceBox A_number;
    public ChoiceBox U_where;
    public ChoiceBox U_tu;
    public ChoiceBox U_number;
    public ListView map_info;
    public ChoiceBox U_tolevel;
    public ChoiceBox C_where;
    public Label watcher;
    public Alert alert;
    public AnchorPane mapAp;
    public BlockViewFactory blockViewFactory;
    public Rectangle pcolor;
    public Label spy;
    public ChoiceBox spy_to;
    public ChoiceBox spy_from;
    public Button butt_OK5;

    private List<BlockView> blocks;
    private boolean is_watcher = false;
    private int player_food;
    private int player_tech;
    private int player_MaxTech;
    private ObservableList<String> list_type_unit;
    private ViewAlter viewAlter;
    boolean up_max_flag;
    boolean is_commit;

    public Button butt_log_out;
    public Button butt_done;
    public Button  butt_up;
    public Button  butt_OK1;
    public Button  butt_OK2;
    public Button  butt_OK3;
    public Button  butt_OK4;
    public ImageView  background;

    Message message;
    ObservableList<String> option_list;
    UpgradeUnitsAction upgradeUnitsAction;
    UpgradeTechLevelAction upgradeTechLevelAction;
    AttackAction attackAction;
    MoveAction moveAction;
    GameInfo gameInfo;
    SpyUpgradeAction spyUpgradeAction;
    SpyMoveAction spyMoveAction;
    CloackingAction cloackingAction;


    @Override
    public void initialize(URL location, ResourceBundle resources){}


    /**
     * play the warn media when error occurs*/
    public void warn_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/battle1.mp3");
                System.out.println(url);
                System.out.println(url.toExternalForm());
                System.out.println(url.toString());
                Media sound = new Media(url.toString());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.5);
                mediaPlayer.setStopTime(Duration.seconds(0.5));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    /**
     * play the OK media when success*/
    public void OK_sound(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                String user_dir = System.getProperty("user.dir");
                URL url = new URL("file:/"+user_dir+"/src/main/resources/win.wav");
                Media sound = new Media(url.toExternalForm());
                MediaPlayer mediaPlayer = new MediaPlayer(sound);
                mediaPlayer.setVolume(0.3);
                mediaPlayer.setStopTime(Duration.seconds(0.3));
                mediaPlayer.play();
                return 0;
            }

            @Override
            protected void succeeded() {
                try {
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    public void createMap(){
        mapAp.getChildren().clear();
        blockViewFactory = new BlockViewFactory(gameInfo,mapAp);
        blocks = blockViewFactory.createMap();
        for(BlockView blockView:blocks){
            blockView.rectangle.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {
                        blockView.rectangle.setStroke(Color.BLACK);
                        blockView.showTerritoryInfo(viewAlter.getPrimaryStage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            blockView.name.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    try {
                        blockView.rectangle.setStroke(Color.BLACK);
                        blockView.showTerritoryInfo(viewAlter.getPrimaryStage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /**
     * init option to the game*/
    public void init() throws Exception {
        if(gameInfo.isGameEnd){ // End of Game
            if(gameInfo.player.isWin()){ // Winner
                alert = new Alert(Alert.AlertType.NONE);
                alert.setTitle("Game End.");
                alert.setContentText("Congratulations! You win the game.");
                alert.getButtonTypes().add(ButtonType.OK);
                alert.showAndWait();
                viewAlter.closeGame();
                return ;
            } else{ // Loser
                alert = new Alert(Alert.AlertType.NONE);
                alert.setTitle("Game End.");
                alert.setContentText("Pity! You lose the game.");
                alert.getButtonTypes().add(ButtonType.OK);
                alert.showAndWait();
                viewAlter.closeGame();
                return ;
            }
        }
        // Watcher
        if(gameInfo.player.isLost()&&!is_watcher){
            alert = new Alert(Alert.AlertType.NONE);
            alert.getButtonTypes().add(ButtonType.OK);
            alert.getButtonTypes().add(ButtonType.CANCEL);
            alert.setTitle("Do you want to be a watcher?");
            alert.setContentText("You lose all your territory. Do you want to be a watcher?");
            Optional<ButtonType> result = alert.showAndWait();
            ButtonType buttonType = result.get();
            if (buttonType == ButtonType.CANCEL){
                viewAlter.closeGame();
            } else if (buttonType == ButtonType.OK) {
                is_watcher = true;
            }
        }
        background.setFitHeight(480);
        background.setFitWidth(640);
        setButtonType(butt_done);
        setButtonType(butt_up);
        setButtonType(butt_OK1);
        setButtonType(butt_OK2);
        setButtonType(butt_OK3);
        setButtonType(butt_OK4);
        setButtonType(butt_OK5);

        butt_log_out.setStyle("-fx-background-color: #7d3d8b; -fx-text-fill: white;-fx-font: 15 arial;");
        butt_log_out.setEffect(new DropShadow());
        butt_log_out.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butt_log_out.setStyle("-fx-background-color: #FFC125;-fx-font:  18 arial;-fx-border-color: red;");
                butt_log_out.setEffect(null);


            }
        });
        butt_log_out.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                butt_log_out.setStyle("-fx-background-color: #7d3d8b; -fx-text-fill: white;-fx-font: 15 arial;");
                butt_log_out.setEffect(new DropShadow());
            }
        });

        L_name.setText(String.valueOf(gameInfo.player.getId()));
        player_food = gameInfo.player.getTotalFoodResource();
        player_tech = gameInfo.player.getTotalTechResource();
        refresh_playerInfo();
        player_MaxTech = gameInfo.player.getTechLevel();
        pcolor.setFill(BlockView.colors.get(gameInfo.player.getId()));
        L_maxTech.setText(String.valueOf(player_MaxTech));
        list_type_unit = FXCollections.observableArrayList("0","1","2","3","4","5","6");
        upgradeUnitsAction = new UpgradeUnitsAction();
        upgradeTechLevelAction = new UpgradeTechLevelAction();
        attackAction = new AttackAction();
        moveAction = new MoveAction();
        spyMoveAction = new SpyMoveAction();
        spyUpgradeAction = new SpyUpgradeAction();
        cloackingAction = new CloackingAction();
        up_max_flag = true;
        is_commit = false;
        option_list = FXCollections.observableArrayList();
        refresh_mapInfo();
        Options.setItems(option_list);
        option_list.add("Your Current Turn Actions:");
        refresh_territory();
        if(is_watcher){
            watcher.setText("(Watcher)");
            Done();
        }
    }

    private void setButtonType(Button button){
        button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white; -fx-font: 15 arial;");
        button.setEffect(new DropShadow());
        button.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: #FFC125;-fx-font: 18 arial;-fx-border-color: green;");
                button.setEffect(null);


            }
        });
        button.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 15 arial;");
                button.setEffect(new DropShadow());
            }
        });
    }


    /**
     * refresh player Info*/
    private void refresh_playerInfo(){
        L_food.setText(String.valueOf(player_food));
        L_tech.setText(String.valueOf(player_tech));
        spy.setText(String.valueOf(gameInfo.player.getSpyUnits().size()));
    }

    /**
     * refresh map Info*/
    private void refresh_mapInfo(){
        ObservableList<String> list_mapInfo = FXCollections.observableArrayList();
        map_info.setItems(list_mapInfo);
        list_mapInfo.add("Here is the map info:");
        for(Territory territory:gameInfo.map.getTerritories()){
            if(territory.isVisible(gameInfo.player.getId())){
                list_mapInfo.add(territory.getName()+":");
                list_mapInfo.add("Unit: "+ territory.getUnitList());
                list_mapInfo.add("Owner: " + territory.getOwnerID());
                list_mapInfo.add("Food production: " + territory.getFoodProduction());
                list_mapInfo.add("Tech production: " + territory.getTechProduction());
            } else{
                list_mapInfo.add(territory.getName()+":???");
            }
        }
    }

    /**
     * refresh territory Info in choice boxes*/
    private void refresh_territory(){
        refresh_Move_territory();
        refresh_Attack_territory();
        refresh_Upgrade_territory();
        refresh_MoveSpy_territory();
        refresh_Cloak_territory();
    }

    private void refresh_MoveSpy_territory() {
        ObservableList<String> list_territory = FXCollections.observableArrayList();
        getHaveUnmovedSpyTerritories(list_territory);
        spy_from.setItems(list_territory);
        spy_from.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(newValue.intValue()==-1){
                    spy_to.setItems(FXCollections.observableArrayList());
                    return ;
                }
                String territory = list_territory.get(newValue.intValue());
                ObservableList<String> new_list_territory = FXCollections.observableArrayList();
                getNeighbourTerritories(territory,new_list_territory);
                spy_to.setItems(new_list_territory);
            }
        });
    }

    /**
     * refresh Move choice boxes*/
    private void refresh_Move_territory(){
        ObservableList<String> list_my_territory = FXCollections.observableArrayList();
        getMyTerritory(list_my_territory);
        M_from.setItems(list_my_territory);
        M_from.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(newValue.intValue()==-1){
                    M_to.setItems(FXCollections.observableArrayList());
                    M_tu.setItems(FXCollections.observableArrayList());
                    M_number.setItems(FXCollections.observableArrayList());
                    return ;
                }
                String my_territory = list_my_territory.get(newValue.intValue());
                Territory territory = gameInfo.map.getTerritoryByName(my_territory);
                ObservableList<String> list_can_arrived_territory = FXCollections.observableArrayList();
                getCanArrivedTerritory(territory,list_can_arrived_territory);
                M_to.setItems(list_can_arrived_territory);
                M_tu.setItems(list_type_unit);
                M_tu.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if(newValue.intValue()==-1){
                            M_number.setItems(FXCollections.observableArrayList());
                            return ;
                        }
                        ObservableList<String> list_unit_number = FXCollections.observableArrayList();
                        for(int i=0;i<=territory.getUnitNumByLevel(newValue.intValue());i++){
                            list_unit_number.add(String.valueOf(i));
                        }
                        M_number.setItems(list_unit_number);
                    }
                });
            }
        });
    }

    /**
     * refresh attach territory*/
    private void refresh_Attack_territory(){
        ObservableList<String> list_can_use_territory = FXCollections.observableArrayList();
        getCanUsedTerritory(list_can_use_territory);
        A_from.setItems(list_can_use_territory);
        A_from.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(newValue.intValue()==-1){
                    A_to.setItems(FXCollections.observableArrayList());
                    A_tu.setItems(FXCollections.observableArrayList());
                    A_number.setItems(FXCollections.observableArrayList());
                    return ;
                }
                String my_territory = list_can_use_territory.get(newValue.intValue());
                Territory territory = gameInfo.map.getTerritoryByName(my_territory);
                ObservableList<String> list_can_attack_territory = FXCollections.observableArrayList();
                getCanAttackTerritory(territory,list_can_attack_territory);
                A_to.setItems(list_can_attack_territory);
                A_tu.setItems(list_type_unit);
                A_tu.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if(newValue.intValue()==-1){
                            A_number.setItems(FXCollections.observableArrayList());
                            return ;
                        }
                        ObservableList<String> list_unit_number = FXCollections.observableArrayList();
                        for(int i=0;i<=territory.getUnitNumByLevel(newValue.intValue());i++){
                            list_unit_number.add(String.valueOf(i));
                        }
                        A_number.setItems(list_unit_number);
                    }
                });
            }
        });
    }

    /**
     * refresh upgrade choice boxes*/
    private void refresh_Upgrade_territory(){
        ObservableList<String> list_my_territory = FXCollections.observableArrayList();
        getMyTerritory(list_my_territory);
        U_where.setItems(list_my_territory);
        U_where.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                if(newValue.intValue()==-1){
                    U_tu.setItems(FXCollections.observableArrayList());
                    U_tolevel.setItems(FXCollections.observableArrayList());
                    U_number.setItems(FXCollections.observableArrayList());
                    return ;
                }
                String my_territory = list_my_territory.get(newValue.intValue());
                Territory territory = gameInfo.map.getTerritoryByName(my_territory);
                U_tu.setItems(list_type_unit);
                U_tu.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if(newValue.intValue()==-1){
                            U_tolevel.setItems(FXCollections.observableArrayList());
                            U_number.setItems(FXCollections.observableArrayList());
                            return ;
                        }
                        ObservableList<String> list_unit_number = FXCollections.observableArrayList();
                        int level = newValue.intValue();
                        for(int i=0;i<=territory.getUnitNumByLevel(level);i++){
                            list_unit_number.add(String.valueOf(i));
                        }
                        U_number.setItems(list_unit_number);
                        U_number.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                            @Override
                            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                                if(newValue.intValue()==-1||newValue.intValue()==0||level==6){
                                    U_tolevel.setItems(FXCollections.observableArrayList());
                                }
                                ObservableList<String> list_to_level = FXCollections.observableArrayList();
                                for(int i=level+1;i<=player_MaxTech;i++) {
                                    list_to_level.add(String.valueOf(i));
                                }
                                if(U_tu.getSelectionModel().getSelectedItem().equals("0")){
                                    list_to_level.add("spy");
                                }
                                U_tolevel.setItems(list_to_level);
                            }
                        });
                    }
                });
            }
        });
    }
    /**
     * refresh Cloak choice boxes*/
    private void refresh_Cloak_territory(){
        ObservableList<String> list_my_territory = FXCollections.observableArrayList();
        getMyTerritory(list_my_territory);

        C_where.setItems(list_my_territory);
    }
    /**
     * forbid all buttons in the game controller*/
    private void forbid_alloption(){
        M_from.setItems(FXCollections.observableArrayList());
        A_from.setItems(FXCollections.observableArrayList());
        U_where.setItems(FXCollections.observableArrayList());
        C_where.setItems(FXCollections.observableArrayList());
        spy_from.setItems(FXCollections.observableArrayList());
        up_max_flag = false;
        is_commit = true;
    }

    /**
     * Finish this turn actions*/
    public void Done() throws Exception {
        if(!is_commit) {
            option_list.add("All actions are submitted!");
            option_list.add("Waiting for other players...");
            forbid_alloption();
            message.updateActionList(attackAction, moveAction, upgradeUnitsAction, upgradeTechLevelAction,
                    spyUpgradeAction,spyMoveAction,cloackingAction);
            Task<Integer> task = new Task<>() {
                @Override
                protected Integer call() throws Exception {
                    viewAlter.waitForUpdate(message);
                    return 0;
                }

                @Override
                protected void succeeded() {
                    try {
                        viewAlter.gotoActionController();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            new Thread(task).start();
        }
    }

    /**
     * Exit the game and go to log in Interface*/
    public void Exit() throws Exception {
        if(!is_commit){
            viewAlter.closeGame();
        } else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Please wait for other players.");
            alert.showAndWait();
        }
    }

    /**
     * submit move actions*/
    public void M_OK() {
        Warnings.setText("");
        if(M_number.getSelectionModel().isSelected(-1)||M_number.getValue().equals("0")){
            warn_sound();
            Warnings.setText("invalid option");
            return ;
        }
        int level = Integer.parseInt((String) M_tu.getValue());
        int number = Integer.parseInt((String) M_number.getValue());
        Territory territory_from = gameInfo.map.getTerritoryByName((String) M_from.getValue());
        Territory territory_to = gameInfo.map.getTerritoryByName((String) M_to.getValue());
        if(territory_from.getUnitNumByLevel(level)<number){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Not Enough Units!");
            alert.showAndWait();
            refresh_territory();
            return ;
        }
        if(gameInfo.map.minimalCostPath(territory_from,territory_to)*number>player_food){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            warn_sound();
            alert.setContentText("Not Enough Food!");
            alert.showAndWait();
            refresh_territory();
            return ;
        }
        player_food -= gameInfo.map.minimalCostPath(territory_from,territory_to)*number;
        moveAction.addOneAction((String) M_from.getValue(),(String) M_to.getValue(),
                Integer.parseInt((String) M_tu.getValue()),
                Integer.parseInt((String) M_number.getValue()));
        territory_from.decreaseUnitsNum(level,number);
        territory_to.increaseUnitsNum(level,number);
        option_list.add("Move " +
                M_number.getValue() +" level "
                + level + " units from "
                + M_from.getValue() + " to " + M_to.getValue());
        OK_sound();
        refresh_playerInfo();
        refresh_territory();
        refresh_mapInfo();
    }

    /**
     * submit attack actions*/
    public void A_OK() {
        Warnings.setText("");
        if(A_number.getSelectionModel().isSelected(-1)||A_number.getValue().equals("0")){
            warn_sound();
            Warnings.setText("invalid option");
            return ;
        }
        int level = Integer.parseInt((String) A_tu.getValue());
        int number = Integer.parseInt((String) A_number.getValue());
        Territory territory_from = gameInfo.map.getTerritoryByName((String) A_from.getValue());
        if(number>player_food){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            warn_sound();
            alert.setContentText("Not Enough Food!");
            alert.showAndWait();
            refresh_territory();
            return ;
        }
        player_food -= number;
        if(territory_from.getUnitNumByLevel(level)<number){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            warn_sound();
            alert.setContentText("Not Enough Units!");
            alert.showAndWait();
            refresh_territory();
            return ;
        }
        attackAction.addOneAction((String) A_from.getValue(),(String) A_to.getValue(),
               Integer.parseInt((String) A_tu.getValue()),
                Integer.parseInt((String) A_number.getValue()));
        territory_from.decreaseUnitsNum(level,number);
        option_list.add("Use " + A_number.getValue() + " level "
                + level + " units from "
                + A_from.getValue() + " attack " + A_to.getValue());
        OK_sound();
        refresh_playerInfo();
        refresh_territory();
        refresh_mapInfo();
    }

    /**
     * submit upgrade actions*/
    public void U_OK() {
        Warnings.setText("");
        if(U_number.getSelectionModel().isSelected(-1)||U_number.getValue().equals("0")||
        U_tolevel.getSelectionModel().isSelected(-1)){
            warn_sound();
            Warnings.setText("invalid option");
            return ;
        }
        if(U_tolevel.getValue().equals("spy")){
            int number = Integer.parseInt((String) U_number.getValue());
            Territory territory = gameInfo.map.getTerritoryByName((String) U_where.getValue());
            int cost = 20;
            if(cost*number>player_tech){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Not Enough Tech Points!");
                alert.showAndWait();
                refresh_territory();
                return ;
            }
            OK_sound();
            player_tech -= cost*number;
            territory.decreaseUnitsNum(0,number);
            for(int i=0;i<number;i++){
                territory.addSpy(new SpyUnit(gameInfo.player.getId(),territory));
            }
            gameInfo.player.addSpy(number,territory);
            option_list.add("Upgrade " + number + " units to spy in "
                    + territory.getName());
            spyUpgradeAction.addOneAction(territory.getName(),number);
            refresh_playerInfo();
            refresh_territory();
            refresh_mapInfo();
            return;
        }
        int level = Integer.parseInt((String) U_tu.getValue());
        int number = Integer.parseInt((String) U_number.getValue());
        int tolevel = Integer.parseInt((String) U_tolevel.getValue());
        Territory territory = gameInfo.map.getTerritoryByName((String) U_where.getValue());
        if(territory.getUnitNumByLevel(level)<number){
            warn_sound();
            Warnings.setText("invalid option");
            return ;
        }
        List<Integer> costs = new ArrayList<>(Arrays.asList(3, 8, 19, 25, 35, 50));
        int cost = 0;
        for(int i=level+1;i<=tolevel;i++){
            cost += costs.get(i-1);
        }
        if(cost*number>player_tech){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            warn_sound();
            alert.setContentText("Not Enough Tech Points!");
            alert.showAndWait();
            refresh_territory();
            return ;
        }
        player_tech -= cost*number;
        territory.decreaseUnitsNum(level,number);
        territory.increaseUnitsNum(tolevel,number);
        upgradeUnitsAction.addOneAction(territory.getName(),level,tolevel,number);
        OK_sound();
        option_list.add("Upgrade " + number +" level "
                        + level + " units to level " + tolevel + " in "
                        + territory.getName());
        refresh_playerInfo();
        refresh_territory();
        refresh_mapInfo();
    }

    public void C_OK() {
        Warnings.setText("");
        if(C_where.getSelectionModel().isSelected(-1)){
            warn_sound();
            Warnings.setText("invalid option");

            return ;
        }
        Territory territory_from = gameInfo.map.getTerritoryByName((String) C_where.getValue());
        if(player_tech < 100 ){ // or tech level less than 3
            warn_sound();
            Warnings.setText("Needs 100 tech number");

            return ;
        }
        if(player_MaxTech < 3 ){ // or tech level less than 3
            warn_sound();
            Warnings.setText("Cloak needs at least 3 Tech Level");

            return ;
        }
        player_tech -= 100;
        cloackingAction.addOneAction(territory_from.getName());
        option_list.add("Cloak the territory " + C_where.getValue());
        refresh_playerInfo();
        refresh_territory();
        refresh_mapInfo();
    }

    public void Spy_OK(ActionEvent actionEvent) {
        Warnings.setText("");
        if(spy_from.getSelectionModel().isSelected(-1)||spy_to.getSelectionModel().isSelected(-1)){
            Warnings.setText("invalid option");
            return ;
        }
        String from = (String) spy_from.getValue();
        String to = (String) spy_to.getValue();
        Territory territory_from = gameInfo.map.getTerritoryByName(from);
        Territory territory_to = gameInfo.map.getTerritoryByName(to);
        SpyUnit spyUnit = territory_from.getCanMoveSpy();
        spyUnit.move(territory_to);
        OK_sound();
        spyMoveAction.addOneAction(from,to);
        option_list.add("You move a spy from "+from+" to "+to);
        refresh_playerInfo();
        refresh_territory();
        refresh_mapInfo();
        createMap();
    }

    /**
     * submit tech level up action*/
    public void up_MaxTech() {
        Warnings.setText("");
        if(up_max_flag){
            List<Integer> costlist = new ArrayList<>(Arrays.asList(50, 75, 125, 200, 300));
            if(player_tech-costlist.get(player_MaxTech-1)>=0){
                OK_sound();
                option_list.add("Your MaxTechLevel is updated.(Valid next turn)");
                upgradeTechLevelAction.setActive(true);
                up_max_flag = false;
                player_tech -= costlist.get(player_MaxTech-1);
                refresh_playerInfo();
            } else{
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Not Enough Tech Points!");
                alert.showAndWait();
            }
        } else{
            Warnings.setText("invalid option");
        }
    }

    /**
     * Set view alter*/
    public void setApp(ViewAlter viewAlter) {
        this.viewAlter = viewAlter;
    }

    /**
     * Set GameInfo*/
    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    /**
     * Set message*/
    public void setMessage(Message message) {
        this.message = message;
    }

    /**
     * Used to list territories belong to the player.*/
    private void getMyTerritory(ObservableList<String> list){
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(territory.getTotalNumOfUnits()!=0){
                list.add(territory.getName());
            }
        }
    }

    /**
     * Used to list territories can attack others.*/
    private void getCanUsedTerritory(ObservableList<String> list){
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(territory.getTotalNumOfUnits()==0){
                continue;
            }
            boolean flag = false;
            for(Territory territory1:territory.getNeighbors()){
                if(territory1.getOwnerID()!=gameInfo.player.getId()){
                    flag = true;
                    break;
                }
            }
            if(flag){
                list.add(territory.getName());
            }
        }
    }

    /**
     * Used to list territories from start can arrive.
     * @param territory the start territory.*/
    private void getCanArrivedTerritory(Territory territory,ObservableList<String> list){
        for(Territory territory1:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(gameInfo.map.isReachableForMove(territory,territory1)&&!territory.equals(territory1)){
                list.add(territory1.getName());
            }
        }
    }

    /**
     * Used to list territories from start can attack.
     * @param territory the start territory.*/
    private void getCanAttackTerritory(Territory territory,ObservableList<String> list){
        for(Territory territory1:gameInfo.map.getTerritories()){
            if(gameInfo.map.isReachableForAttack(territory,territory1)){
                list.add(territory1.getName());
            }
        }
    }

    private void getHaveUnmovedSpyTerritories(ObservableList<String> list){
        for(SpyUnit spyUnit:gameInfo.player.getSpyUnits()){
            if(spyUnit.isAvailableToMove()&&!list.contains(spyUnit.getCurrentTerritory().getName())){
                list.add(spyUnit.getCurrentTerritory().getName());
            }
        }
    }

    private void getNeighbourTerritories(String string,ObservableList<String> list){
        Territory t = gameInfo.map.getTerritoryByName(string);
        for(Territory territory:t.getNeighbors()){
            list.add(territory.getName());
        }
    }


}
