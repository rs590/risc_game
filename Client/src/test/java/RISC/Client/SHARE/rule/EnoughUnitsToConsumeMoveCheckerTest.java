package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughUnitsToConsumeMoveCheckerTest {
    @Test
    public void test_enoughUnitsToConsumeMove() {
        EnoughUnitsToConsumeMoveChecker enoughUnitsToConsumeMoveChecker = new EnoughUnitsToConsumeMoveChecker(null);
        assertThrows(NullPointerException.class, ()->enoughUnitsToConsumeMoveChecker.checkMyRule(null, null, null));
    }

}