package RISC.Client.SHARE.game;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

class SpyUnitTest {

    static GameMapFactory factory = new GameMapFactory();

    ArrayList<Integer> createPlayerList(int playerNum) {
        ArrayList<Integer> playerIDs = new ArrayList<>();
        playerIDs.add(0);
        playerIDs.add(1);
        switch (playerNum) {
            case 5:
                playerIDs.add(4);
            case 4:
                playerIDs.add(3);
            case 3:
                playerIDs.add(2);
                break;
        }
        return playerIDs;
    }

    @Test
    public void test_spy() {
        GameMap map = factory.createMap(2, createPlayerList(2));
        SpyUnit spyUnit = new SpyUnit(0, map.getTerritoryByName("Narnia"));
        spyUnit.move(map.getTerritoryByName("Midkemia"));
    }
}