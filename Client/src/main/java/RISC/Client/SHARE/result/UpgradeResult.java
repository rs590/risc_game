package RISC.Client.SHARE.result;

import RISC.Client.SHARE.game.Territory;

import java.io.Serializable;

public class UpgradeResult implements Serializable {
    public Territory where;
    public int level_from;
    public int level_to;
    public int num;
    public int techcost;

    public UpgradeResult(Territory where, int level_from, int level_to, int num, int techcost) {
        this.where = where;
        this.level_from = level_from;
        this.level_to = level_to;
        this.num = num;
        this.techcost = techcost;
    }
}
