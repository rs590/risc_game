package RISC.Client;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * The Client APP main function
 * extends the JavaFX Application*/
public class ClientApp extends Application{
    private Stage stage;
    private ViewAlter viewAlter;

    public static void main(String[] args){
        Application.launch();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        stage.setTitle("RISC");
        this.viewAlter = new ViewAlter(stage);
        viewAlter.gotoLogin();
        stage.setHeight(400);
        stage.setHeight(600);
        stage.setResizable(false);
        stage.show();
    }
}