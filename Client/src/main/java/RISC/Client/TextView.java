package RISC.Client;

import RISC.Client.SHARE.action.*;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Client Text View Class
 * Help to print the information and collect player input.
 * gameInfo: the information of current game.
 * reader: local input reader.
 * is_watcher: judge if the player is dead and watch game as a watcher.
 * attackAction: player creates the attackActions.
 * moveAction: player creates the moveActions.*/
public class TextView {
    GameInfo gameInfo;
    BufferedReader reader;
    boolean is_watcher;
    AttackAction attackAction;
    MoveAction moveAction;
    UpgradeUnitsAction upgradeUnitsAction;
    UpgradeTechLevelAction upgradeTechLevelAction;

    /**
     * Constructor
     *
     * @param gameInfo the information of current game.
     * @param reader   local input reader.
     */
    public TextView(GameInfo gameInfo, BufferedReader reader) {
        this.gameInfo = gameInfo;
        this.reader = reader;
        is_watcher = false;
        this.attackAction = new AttackAction();
        this.moveAction = new MoveAction();
    }

    /**
     * Used to update gameInfo from server each turn.
     * @param gameInfo the information of this turn.*/
    public boolean updatGameInfo(GameInfo gameInfo) throws IOException {
        this.gameInfo = gameInfo;

        if (gameInfo.isGameEnd) {
            return true;
        }

        this.attackAction = new AttackAction();
        this.moveAction = new MoveAction();
        this.upgradeTechLevelAction = new UpgradeTechLevelAction();
        this.upgradeUnitsAction = new UpgradeUnitsAction();
        if (gameInfo.player.isLost() && !is_watcher) {
            is_watcher = true;
            return printDeadInfo();
        } else if (gameInfo.player.isWin()) {
            System.out.println("<<Congratulations! You win the game.>>");
            return true;
        } else return gameInfo.isGameEnd;
    }

    public LoginAction createLoginAction() throws IOException {
        System.out.println("Please enter your player id:");
        String line;
        line = reader.readLine();
        while (!isNumeric(line)) {
            System.out.println("Your input is not a number. Please try again.");
            line = reader.readLine();
        }
        int playerID = Integer.parseInt(line);
        System.out.println("Please enter your password:");
        String pass = reader.readLine();

        return new LoginAction(playerID, pass);
    }

    public ChooseGameAction createChooseGameAction() throws IOException {
        System.out.println("Please enter the game id:");
        String line;
        line = reader.readLine();
        while (!isNumeric(line)) {
            System.out.println("Your input is not a number. Please try again.");
            line = reader.readLine();
        }
        int gameID = Integer.parseInt(line);
        return new ChooseGameAction(gameID);
    }

    /**
     * Used to create SetUnitAction. Help player sets units at the beginning.
     *
     * @return the SetUnitAction will be stored in the message send to Server.
     */
    public SetUnitAction createSetUnitAction() throws IOException {
        System.out.println("Please set your units: E.g. Narnia 10 Midkemia 12 Oz 8");
        int unit_num = 30;
        List<Integer> unitToput = new ArrayList<>();
        List<String> territories = new ArrayList<>();
        List<List<Integer>> allUnits = new ArrayList<>();
        String line;
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            System.out.println("How many units you want to put to " + territory.getName() + "?"+
                    "(You have "+unit_num+" units left.)");
            line = reader.readLine();
            while(!isNumeric(line)){
                System.out.println("Your input is not a number. Please try again.");
                line = reader.readLine();
            }
            int unit = Integer.parseInt(line);
            while(unit>unit_num||unit<0){
                System.out.println("Your input is out of range. Please try again.");
                line = reader.readLine();
                while (!isNumeric(line)) {
                    System.out.println("Your input is not a number. Please try again.");
                    line = reader.readLine();
                }
                unit = Integer.parseInt(line);
            }
            unitToput.add(unit);
            unit_num -= unit;
            territories.add(territory.getName());
        }

        for (int unit : unitToput) {
            List<Integer> temp = new ArrayList<>();
            temp.add(unit);
            for (int i = 0; i < 6; i++) {
                temp.add(0);
            }
            allUnits.add(temp);
        }

        return new SetUnitAction(territories, allUnits);
    }

    /**
     * Used to print the map and player's territory info.*/
    public void printMapInfo() {
        for (Player player : gameInfo.playerList) {
            System.out.println("Player " + player.getId() + ":");
            System.out.println("food: " + player.getTotalFoodResource());
            System.out.println("tech: " + player.getTotalTechResource());
            System.out.println("tech level: " + player.getTechLevel());
            System.out.println("----------------------------");
            for (Territory territory : gameInfo.map.getTerritoriesForPlayer(player.getId())) {
                System.out.println(territory);
            }
        }
    }

    /**
     * Used to show the text of each turn and collect player's input.*/
    public void showText() throws IOException {
        printMapInfo();
        createActionList();
    }

    /**
     * Used to print game over info for player.*/
    public void printEndInfo(){
        //whole game is over
        if(gameInfo.isGameEnd) {
            int winner = -1;
            for(Player p:gameInfo.playerList){
                if(p.isWin()){
                    winner = p.getId();
                }
            }
            System.out.println("<<Game is Over. Winner is "+winner+" >>");
        }
        System.out.println("<<Thank U for playing. The game will over soon...>>");
    }

    /**
     * Used to print lose info for player.
     * @return if player want to exit the game immediately.*/
    private boolean printDeadInfo() throws IOException {
        System.out.println("<<You lose all your territories. You are lose.>>");
        System.out.println("<<Do you want to leave game? (Y/N) >>");
        String s = reader.readLine();
        s = s.toUpperCase();
        while(!s.equals("Y")&&!s.equals("N")){
            System.out.println("Your input is invalid. Please try again.");
            s = reader.readLine().toUpperCase();
        }
        return s.equals("Y");
    }

    /**
     * Used to print the choice information for player before each action.*/
    private void printHeader() {
        System.out.println("<<You are " + gameInfo.player.getId() + ", what would you like to do?>>");
        System.out.println("(M)ove");
        System.out.println("(A)ttack");
        System.out.println("(UU)Upgrade Units");
        System.out.println("(UT)Upgrade Tech");
        System.out.println("(D)one");

    }

    /**
     * Used to collect player input to crate different actions.*/
    private void createActionList() throws IOException {
        printHeader();
        String s = reader.readLine();
        s = s.toUpperCase();
        //Not committed
        while(!(s.equals("D")||s.equals("DONE"))){
            //choose a move action
            if(s.equals("M")||s.equals("MOVE")){
                s = createMoveAction().toUpperCase();
            }
            //choose an attack action
            else if (s.equals("A") || s.equals("ATTACK")) {
                s = createAttackAction().toUpperCase();
            } else if (s.equals("UU") || s.equals("UPGRADE UNIT")) {
                s = createUpgradeUnitAction().toUpperCase();
            } else if (s.equals("UT") || s.equals("UPGRADE TECH")) {
                s = createUpgradeTechAction().toUpperCase();
            }
            //wrong input
            else {
                System.out.println("Your action is invalid. Please Try again.");
                s = reader.readLine().toUpperCase();
            }
        }
    }

    public String createUpgradeUnitAction() throws IOException {
        System.out.println("Which territory do you want to upgrade?");
        printMyTerritory();
        String input = reader.readLine();
        Territory from = getTerritory(input);
        while (from == null) {
            System.out.println("There is something wrong in your input territory name. " +
                    "Please check and input again.");
            input = reader.readLine();
            from = getTerritory(input);
        }

        System.out.println("<<which level you want to upgrade?>>");
        input = reader.readLine();
        while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0 || from.getUnitList().get(Integer.parseInt(input)) <= 0) {
            System.out.println("Your input is not valid. Please try again.");
            input = reader.readLine();
        }
        int unitLevelFrom = Integer.parseInt(input);

        System.out.println("<<which level you want to upgrade to?>>");
        input = reader.readLine();
        while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0) {
            System.out.println("Your input is not valid. Please try again.");
            input = reader.readLine();
        }
        int unitLevelTo = Integer.parseInt(input);

        System.out.println("<<how many units you wanna upgrade?>>");
        input = reader.readLine();
        while (!isNumeric(input) || !hasEnoughUnit(from, Integer.parseInt(input), unitLevelFrom)) {
            System.out.println("Your input is not valid. Please try again.");
            input = reader.readLine();
        }
        int num = Integer.parseInt(input);

        from.updateUnitList(-num, unitLevelFrom);
        from.updateUnitList(num, unitLevelTo);

        this.upgradeUnitsAction.addOneAction(from.getName(), unitLevelFrom, unitLevelTo, num);
        printHeader();
        return reader.readLine();
    }

    /**
     * Used to create a move action by player's input.
     *
     * @return next action choice.
     */
    private String createMoveAction() throws IOException {
        System.out.println("Which territory's unit do you want to move?");
        printMyTerritory();

        //define from territory
        String input = reader.readLine();
        Territory from = getTerritory(input);
        while(from==null){
            System.out.println("There is something wrong in your input territory name. " +
                    "Please check and input again.");
            input = reader.readLine();
            from = getTerritory(input);
        }

        //define to territory
        System.out.println("Where you want to move your unit to?");
        printCanArrivedTerritory(from);
        input = reader.readLine();
        Territory to = getCanMoveTerritory(input, from);
        while (to == null) {
            System.out.println("There is something wrong in your input territory name. " +
                    "Please check and input again.");
            input = reader.readLine();
            to = getCanMoveTerritory(input, from);
        }

        System.out.println("<<which level you want to move?>>");
        input = reader.readLine();
        while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0) {
            System.out.println("Your input is not valid. Please try again.");
            input = reader.readLine();
        }
        int unitLevel = Integer.parseInt(input);

        //define unit number to move
        System.out.println("How many units you want to move?");
        input = reader.readLine();
        while (!isNumeric(input)) {
            System.out.println("Your input is not a number. Please try again.");
            input = reader.readLine();
        }
        int num = Integer.parseInt(input);
        while (!hasEnoughUnit(from, num, unitLevel)) {
            System.out.println("There is not enough units on " + from.getName() + "." +
                    " Please input another number.");
            System.out.println("<<which level you want to move?>>");
            input = reader.readLine();
            while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0) {
                System.out.println("Your input is not valid. Please try again.");
                input = reader.readLine();
            }
            unitLevel = Integer.parseInt(input);
            System.out.println("<<How many units you want to move?>>");
            input = reader.readLine();
            while (!isNumeric(input)) {
                System.out.println("Your input is not a number. Please try again.");
                input = reader.readLine();
            }
            num = Integer.parseInt(input);
        }

        from.updateUnitList(-num, unitLevel);
        to.updateUnitList(num, unitLevel);

        moveAction.addOneAction(from.getName(), to.getName(), unitLevel, num);
        printHeader();
        return reader.readLine();
    }

    private String createUpgradeTechAction() throws IOException {
        int[] cost = {50, 75, 125, 200, 300};
        if (gameInfo.player.getTotalTechResource() < cost[gameInfo.player.getTechLevel() - 1]) {
            System.out.println("not enough tech resource!");
            printHeader();
            return reader.readLine();
        }
        this.upgradeTechLevelAction.setActive(true);
        printHeader();
        return reader.readLine();
    }

    /**
     * Used to create an attack action by player's input.
     *
     * @return next action choice.
     */
    private String createAttackAction() throws IOException {
        System.out.println("<<Which territory unit do you want to use to attack?>>");
        printCanUsedTerritory();
        String input = reader.readLine();
        Territory from = getCanUsedTerritory(input);
        while (from == null) {
            System.out.println("<<There is something wrong in your input territory name. " +
                    "Please check and input again.>>");
            input = reader.readLine();
            from = getCanUsedTerritory(input);
        }
        System.out.println("<<Where you want to attack? (Only neighbour territory is available)>>");
        printCanAttackTerritory(from);
        input = reader.readLine();
        Territory to = getCanAttackTerritory(input, from);
        while (to == null) {
            System.out.println("<<There is something wrong in your input territory name. " +
                    "Please check and input again.>>");
            input = reader.readLine();
            to = getCanAttackTerritory(input, from);
        }

        System.out.println("which level you want to move?");
        input = reader.readLine();
        while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0) {
            System.out.println("Your input is not valid. Please try again.");
            input = reader.readLine();
        }
        int unitLevel = Integer.parseInt(input);

        System.out.println("<<How many units you want to attack?>>");
        input = reader.readLine();
        while (!isNumeric(input)) {
            System.out.println("<<Your input is not a number. Please try again.>>");
            input = reader.readLine();
        }
        int num = Integer.parseInt(input);
        while (!hasEnoughUnit(from, num, unitLevel)) {
            System.out.println("<<There is not enough units on " + from.getName() + "." +
                    " Please input another number.>>");

            System.out.println("<<which unit level you want to attack?>>");
            input = reader.readLine();
            while (!isNumeric(input) || Integer.parseInt(input) > 6 || Integer.parseInt(input) < 0) {
                System.out.println("Your input is not valid. Please try again.");
                input = reader.readLine();
            }
            unitLevel = Integer.parseInt(input);

            System.out.println("<<How many units you want to attack?>>");
            input = reader.readLine();
            while (!isNumeric(input)) {
                System.out.println("<<Your input is not a number. Please try again.>>");
                input = reader.readLine();
            }
            num = Integer.parseInt(input);
        }
        from.updateUnitList(-num, unitLevel);
        attackAction.addOneAction(from.getName(), to.getName(), unitLevel, num);
        printHeader();
        return reader.readLine();
    }

    /**
     * Used to judge if the input string is a number.
     * @param str the string needs to be judged.
     * @return true if string is a number, else false.*/
    private static boolean isNumeric(String str) {
        String regEx = "^[0-9]+$";
        Pattern pat = Pattern.compile(regEx);
        Matcher mat = pat.matcher(str);
        return mat.find();
    }

    /**
     * Used to get the territory by user input.
     * @param input the player input territory name.
     * @return null if can't find such territory, else the territory.*/
    private Territory getTerritory(String input){
        for(Territory territory: gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(territory.getName().equals(input)){
                return territory;
            }
        }
        return null;
    }

    /**
     * Used to get the territory where player can move his units to.
     * @param input the player input destination name.
     * @param t the territory where the units from.
     * @return null if can't find such territory, else the destination territory.*/
    private Territory getCanMoveTerritory(String input,Territory t){
        for(Territory territory: gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(territory.getName().equals(input)){
                if(gameInfo.map.isReachableForMove(t,territory)){
                    return territory;
                }
            }
        }
        return null;
    }

    /**
     * Used to get the territory where player can use his units to attack.
     * @param input the player input target territory name.
     * @param territory the base territory.
     * @return null if no such territory, else the target territory.*/
    private Territory getCanAttackTerritory(String input,Territory territory){
        for(Territory t:gameInfo.map.getTerritories()){
            if(t.getName().equals(input)){
                if(gameInfo.map.isReachableForAttack(territory,t)){
                    return t;
                }
            }
        }
        return null;
    }

    /**
     * Used to get the territory where player can use units to attack.
     * @param input the player input target territory name.
     * @return null if no such territory, else the target territory.*/
    private Territory getCanUsedTerritory(String input){
        for(Territory territory: gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            boolean flag = false;
            for(Territory territory1:territory.getNeighbors()){
                if(territory1.getOwnerID()!=gameInfo.player.getId()){
                    flag = true;
                    break;
                }
            }
            if(flag&&territory.getName().equals(input)){
                return territory;
            }
        }
        return null;
    }

    /**
     * Used to list territories belong to the player.*/
    private void printMyTerritory(){
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())) {
            System.out.println(territory.getName()+"("+territory.getUnitList()+").");
        }
    }

    /**
     * Used to list territories can attack others.*/
    private void printCanUsedTerritory(){
        for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            boolean flag = false;
            for(Territory territory1:territory.getNeighbors()){
                if(territory1.getOwnerID()!=gameInfo.player.getId()){
                    flag = true;
                    break;
                }
            }
            if(flag){
                System.out.println(territory.getName()+"("+territory.getUnitsNum()+").");
            }
        }
    }

    /**
     * Used to list territories from start can arrive.
     * @param territory the start territory.*/
    private void printCanArrivedTerritory(Territory territory){
        for(Territory territory1:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            if(gameInfo.map.isReachableForMove(territory,territory1)){
                System.out.println(territory1.getName()+"("+territory1.getUnitsNum()+").");
            }
        }
    }

    /**
     * Used to list territories from start can attack.
     * @param territory the start territory.*/
    private void printCanAttackTerritory(Territory territory){
        System.out.println("<<Here is the territories you can attack:>>");
        for(Territory territory1:gameInfo.map.getTerritories()){
            if(gameInfo.map.isReachableForAttack(territory,territory1)){
                System.out.println(territory1.getName()+"("+territory1.getUnitsNum()+")"
                        + " belongs to " + territory1.getOwnerID());
            }
        }
    }

    /**
     * Used to judge if there is enough units in territory.
     *
     * @param des the start territory.
     * @param num the number of units wants to move.
     * @return true if there is enough unit, else false.
     */
    private boolean hasEnoughUnit(Territory des, int num, int unitLevel) {
        return des.getUnitList().get(unitLevel) >= num;
    }
}
