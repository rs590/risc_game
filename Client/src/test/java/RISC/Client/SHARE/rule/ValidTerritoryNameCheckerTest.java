package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidTerritoryNameCheckerTest {
    @Test
    public void test_validTerritoryName() {
        ValidTerritoryNameChecker validTerritoryNameChecker = new ValidTerritoryNameChecker(null);
        assertThrows(NullPointerException.class, ()->validTerritoryNameChecker.checkMyRule(null, null, null));
    }
}