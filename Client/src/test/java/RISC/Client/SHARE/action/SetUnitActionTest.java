package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SetUnitActionTest {
    @Test
    public void test_setUnit() {
        SetUnitAction setUnitAction = new SetUnitAction(null, null);
        assertThrows(NullPointerException.class, ()->setUnitAction.execute(null, null, null));
    }
}