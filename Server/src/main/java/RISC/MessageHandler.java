package RISC;

import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.utility.SerializeUtility;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * This class is used for handling message from a single player
 *
 * @author cirun zhang
 */
class MessageHandler extends Thread {
    private Player player;
    private Socket sock; //socket for communication
    protected CyclicBarrier actionBarrier; //cyclic barrier for synchronization
    private MessageReceiver parentMessageReceiver;
    private InputStream input;  //input stream
    private OutputStream output; //output stream

    /**
     * Constructor
     *
     * @param player                player listen to
     * @param sock                  socket for communication
     * @param parentMessageReceiver message receiver
     * @param actionBarrier         the action barrier for synchronization
     */
    public MessageHandler(Player player, Socket sock, MessageReceiver parentMessageReceiver, CyclicBarrier actionBarrier) {
        this.player = player;
        this.sock = sock;
        this.parentMessageReceiver = parentMessageReceiver;
        this.actionBarrier = actionBarrier;
    }

    @Override
    public void run() {
        try {
            this.input = this.sock.getInputStream();
            this.output = this.sock.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        handle();
    }

    /**
     * Close the connection
     */
    private void closeSession() {
        try {
            this.input.close();
            this.output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Listen for incoming message
     */
    private void handle() {
        SerializeUtility utility = new SerializeUtility();
        while (true) {
            try {
                Message message = utility.recvMessage(input);
                if (message == null) { //socket closed, thread exit
                    player.setConnected(false);
                    System.out.println("player " + player.getId() + " has disconnected");
                    return;
                }
                this.parentMessageReceiver.addMessage(player.getId(), message); //add to messages
                this.actionBarrier.await(); //wait barrier
            } catch (BrokenBarrierException | InterruptedException e) { //game end? Not sure
                e.printStackTrace();
                closeSession();
                return;
            }
        }
    }
}
