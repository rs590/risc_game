package RISC.Client.SHARE.rule;


import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

/**
 * Check if two territory belong to the same owner
 */
public class SameOwnerChecker extends RuleChecker {

    public SameOwnerChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        for (int i = 0; i < action.from.size(); i++) {
            String from = action.from.get(i);
            String to = action.to.get(i);
            Territory source = gameMap.getTerritoryByName(from);
            Territory dest = gameMap.getTerritoryByName(to);
            if (source.getOwnerID() != dest.getOwnerID()) {
                return "err: " + from + " and " + to + " had different player ID";
            }
        }
        return null;
    }
}
