package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughUnitsToConsumeAttackCheckerTest {
    @Test
    public void test_enoughUnitsToConsumeAttack() {
        EnoughUnitsToConsumeAttackChecker enoughUnitsToConsumeAttackChecker = new EnoughUnitsToConsumeAttackChecker(null);
        assertThrows(NullPointerException.class, ()->enoughUnitsToConsumeAttackChecker.checkMyRule(null, null, null));
    }

}