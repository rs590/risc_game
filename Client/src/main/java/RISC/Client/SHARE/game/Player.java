package RISC.Client.SHARE.game;


import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class Player implements Serializable {
    private final int id; //player id
    private boolean isLost; //is player lost
    private boolean isWin; //is player win
    private LinkedHashSet<Territory> territoryList; //all territories
    private int activeGameID; //current active game
    private volatile boolean isConnected; //is the player currently online

    // EVAL2-Resources
    private int totalFoodResource;
    private int totalTechResource;
    private int techLevel;

    //EVAL-3
    private Set<SpyUnit> spyUnits = new HashSet<>();

    private Set<Territory> visited = new HashSet<>();

    public void updateVisited(GameMap gameMap) {
        for (Territory territory : gameMap.getTerritoriesForPlayer(id)) {
            visited.add(territory);
        }
        for (SpyUnit spyUnit : spyUnits) {
            visited.add(spyUnit.getCurrentTerritory());
        }
    }

    public Set<Territory> getVisited() {
        return visited;
    }

    public int getSpyByTerritory(Territory territory) {
        int count = 0;
        for (SpyUnit spyUnit : spyUnits) {
            if (spyUnit.getCurrentTerritory().equals(territory)) {
                count++;
            }
        }
        return count;
    }

    public void addSpy(int num, Territory territory) {
        for (int i = 0; i < num; i++) {
            SpyUnit spy = new SpyUnit(id, territory);
            spyUnits.add(spy);
            territory.addSpy(spy);
        }
    }

    public void setAllSpyAvaliableToMove() {
        for (SpyUnit spyUnit : spyUnits) {
            spyUnit.setAvailableToMove(true);
        }
    }

    public Set<SpyUnit> getSpyUnits() {
        return spyUnits;
    }

    //added by cz174 3.26
    public Results results = new Results();


    //Todo cz174 will do this after yuqi merged spy to it.
//    public Set<Territory> allVisited = new HashSet<>();
//
//    public void updateVisited(GameMap map) {
//        this.allVisited.addAll(map.getTerritoriesForPlayer(id));
//        for (SpyUnit spy : allSpys) {
//          ....
//        }
//    }
    public void clearAllResults() {
       results.clearAllResult();
    }

    public void addResourcesFromTerritories() {
        for (Territory territory : territoryList) {
            totalFoodResource += territory.getFoodProduction();
            totalTechResource += territory.getTechProduction();
        }
    }

    public Player(int id) {
        this.id = id;
        this.totalFoodResource = 50;
        this.totalTechResource = 50;
        this.techLevel = 1;
    }

    public Player(int id, int activeGameID) {
        this.id = id;
        this.isLost = false;
        this.isWin = false;
        this.activeGameID = activeGameID;
        this.isConnected = false;
        this.totalFoodResource = 50;
        this.totalTechResource = 50;
        this.techLevel = 1;
    }

    public Player(int id, LinkedHashSet<Territory> territoryList) {
        this.id = id;
        this.isLost = false;
        this.isWin = false;
        this.territoryList = territoryList;
        this.isConnected = false;
        this.totalFoodResource = 50;
        this.totalTechResource = 50;
        this.techLevel = 1;
    }

    public int getId() {
        return id;
    }

    public void setTerritoryList(LinkedHashSet<Territory> territoryList) {
        this.territoryList = territoryList;
    }

    public void addTerritory(Territory territory) {
        this.territoryList.add(territory);
    }

    public LinkedHashSet<Territory> getTerritoryList() {
        return territoryList;
    }

    public boolean isLost() {
        return isLost;
    }

    public void setLost(boolean lost) {
        isLost = lost;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public int getActiveGameID() {
        return activeGameID;
    }

    public void setActiveGameID(int activeGameID) {
        this.activeGameID = activeGameID;
    }

    public int getTotalFoodResource() {
        return totalFoodResource;
    }

    public void setTotalFoodResource(int totalFoodResource) {
        this.totalFoodResource = totalFoodResource;
    }

    public int getTotalTechResource() {
        return totalTechResource;
    }

    public void setTotalTechResource(int totalTechResource) {
        this.totalTechResource = totalTechResource;
    }

    public int getTechLevel() {
        return techLevel;
    }

    public void setTechLevel(int techLevel) {
        this.techLevel = techLevel;
    }

    public void increaseTotalFoodResource(int num) {
        this.totalFoodResource += num;
    }

    public void decreaseTotalFoodResource(int num) {
        this.totalFoodResource -= num;
    }

    public void increaseTotalTechResource(int num) {
        this.totalTechResource += num;
    }

    public void decreaseTotalTechResource(int num) {
        this.totalTechResource -= num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        return id == player.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
