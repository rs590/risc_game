package RISC.Client.SHARE.rule;



import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;

import java.util.List;

public class EnoughTechToUpgradePlayerChecker extends RuleChecker {

    public EnoughTechToUpgradePlayerChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    /**
     * Check the action is valid or not
     *
     * @param gameMap game map
     * @param player
     * @param action  action
     * @return valid or not
     */
    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        // input level to update
        // example: current level: 0 -> update to 2
        int curLevel = player.getTechLevel();
        List<Integer> updateTechCostList = action.updateTechCostList;

        // False: update tech > current tech
        if (updateTechCostList.get(curLevel - 1) > player.getTotalTechResource()) {
            return "err: need " + updateTechCostList.get(curLevel - 1) + " tech resources, but got " + player.getTotalTechResource();
        }
        return null;
    }
}
