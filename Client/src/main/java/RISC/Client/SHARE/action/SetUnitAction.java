package RISC.Client.SHARE.action;


import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.rule.ValidTerritoryNameChecker;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SetUnitAction extends Action {
    public SetUnitAction(List<String> to, List<List<Integer>> units) {
        super(new ArrayList<>(), to, units);
        this.ruleChecker = new ValidTerritoryNameChecker(null);
    }

    /**
     * Execute the set unit action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        String err = ruleChecker.isValid(gameMap, player, this);
        if (err != null) {
            return err;
        }
        for (int i = 0; i < this.to.size(); i++) {
            String territoryName = this.to.get(i);
            List<Integer> unitList = this.units.get(i);
            for (int level = 0; level < unitList.size(); ++level) {
                gameMap.getTerritoryByName(territoryName).updateUnitList(unitList.get(level), level);
            }
        }
        return null;
    }
}
