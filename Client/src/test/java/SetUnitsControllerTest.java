import RISC.Client.BlockViewFactory;
import RISC.Client.InterfaceController;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Territory;
import RISC.Client.SetUnitsController;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SetUnitsControllerTest {
    // assertEquals(10, b1.getWidth());
    // assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20,inBoundsRuleChecker,'X'));
    @Test
    public void test_SetUnitsController() {
        List<String> territories= new ArrayList<>();;
        GameInfo gameInfo =  new GameInfo(true);
        territories.add("Test1");
        territories.add("Test2");
        territories.add("Test3");
        territories.add("Test4");
        for(int i=0;i<9;i++){
            gameInfo.map.getTerritoryByName(territories.get(i)).increaseUnitsNum(0,i);
        }
        for(Territory territory: gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
            territories.add(territory.getName());
        }
        SetUnitsController blockViewFactory = new SetUnitsController();

        assertEquals(false, SetUnitsController.isNumeric("String str"));
        assertEquals(true, SetUnitsController.isNumeric("9"));
        //assertThrows(IllegalArgumentException.class, () -> new BlockViewFactory());
    }

}
