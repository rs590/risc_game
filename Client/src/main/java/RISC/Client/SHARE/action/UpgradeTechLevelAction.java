package RISC.Client.SHARE.action;



import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.rule.EnoughTechToUpgradePlayerChecker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class UpgradeTechLevelAction extends Action {

    private boolean active;

    public UpgradeTechLevelAction() {
        super();
        this.ruleChecker = new EnoughTechToUpgradePlayerChecker(null);
        this.updateTechCostList = new ArrayList<>(Arrays.asList(50, 75, 125, 200, 300));    // cost of upgrade
        this.active = false;
    }

    /**
     * Execute the upgrade tech level action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        String err = ruleChecker.isValid(gameMap, player, this);

        if (err != null) {
            return err;
        }

        int curLevel = player.getTechLevel();

        player.decreaseTotalTechResource(updateTechCostList.get(curLevel - 1));
        player.setTechLevel(player.getTechLevel() + 1);
        return null;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
