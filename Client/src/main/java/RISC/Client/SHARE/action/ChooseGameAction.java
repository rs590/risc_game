package RISC.Client.SHARE.action;

import java.io.Serializable;

public class ChooseGameAction implements Serializable {
    public int gameID;
    // chose game room by gameID
    public ChooseGameAction(int gameID) {
        this.gameID = gameID;
    }
}
