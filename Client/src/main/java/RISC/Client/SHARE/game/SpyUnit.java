package RISC.Client.SHARE.game;

import java.io.Serializable;

public class SpyUnit implements Serializable {
    private int ownerID;
    private Territory currentTerritory;
    private boolean availableToMove;

    public SpyUnit(int ownerID, Territory currentTerritory) {
        this.ownerID = ownerID;
        this.currentTerritory = currentTerritory;
        this.availableToMove = true;
    }

    public void move(Territory targetTerritory) {
        currentTerritory.removeSpy(this);
        targetTerritory.addSpy(this);
        this.currentTerritory = targetTerritory;
        availableToMove = false;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public Territory getCurrentTerritory() {
        return currentTerritory;
    }

    public void setCurrentTerritory(Territory currentTerritory) {
        this.currentTerritory = currentTerritory;
    }

    public boolean isAvailableToMove() {
        return availableToMove;
    }

    public void setAvailableToMove(boolean availableToMove) {
        this.availableToMove = availableToMove;
    }
}
