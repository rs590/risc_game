package RISC.Client.SHARE.utility;



import RISC.Client.SHARE.action.ChooseGameRoomInfo;
import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.GameInfo;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * This class is used to send/receive messages through network
 *
 * @author cirun zhang
 */
public class SerializeUtility {
    public void sendText(String message, OutputStream output) throws IOException {
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(output, StandardCharsets.UTF_8));
        writer.write(message);
        writer.newLine(); //important: need this to send
        writer.flush();
        System.out.println("sent " + message);
    }

    public String recvText(InputStream input) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
        String message = null;

        try {
            message = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return message;
    }


    public boolean sendGameInfo(GameInfo gameInfo, OutputStream output) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(output);
            writer.writeObject(gameInfo);
            writer.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * Receive a game info object
     *
     * @param input input stream
     * @return game info object
     */
    public GameInfo recvGameInfo(InputStream input) {
        GameInfo gameInfo = null;
        try {
            ObjectInputStream reader = new ObjectInputStream(input);
            gameInfo = (GameInfo) reader.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        assert gameInfo != null;
        return gameInfo;
    }

    /**
     * Send message object
     *
     * @param message Message object to send
     * @param output  output stream
     */
    public void sendMessage(Message message, OutputStream output) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(output);
            writer.writeObject(message);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive a message object
     *
     * @param input input stream
     * @return message object
     */
    public Message recvMessage(InputStream input) {
        Message message = null;
        try {
            ObjectInputStream reader = new ObjectInputStream(input);
            message = (Message) reader.readObject();
        } catch (IOException e) {
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        assert message != null;
        return message;
    }

    public boolean sendChooseGameRoomInfo(ChooseGameRoomInfo chooseGameRoomInfo, OutputStream output) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(output);
            writer.writeObject(chooseGameRoomInfo);
            writer.flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ChooseGameRoomInfo recvChooseGameRoomInfo(InputStream input) {
        ChooseGameRoomInfo chooseGameRoomInfo = null;
        try {
            ObjectInputStream reader = new ObjectInputStream(input);
            chooseGameRoomInfo = (ChooseGameRoomInfo) reader.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        assert chooseGameRoomInfo != null;
        return chooseGameRoomInfo;
    }
}
