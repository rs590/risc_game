package RISC;

import java.util.HashMap;
import java.util.Map;

public class UserDB {
    private Map<Integer, String> accountToPass;

    public UserDB() {
        accountToPass = new HashMap<>();
        accountToPass.put(0, "000");
        accountToPass.put(1, "111");
        accountToPass.put(2, "222");
        accountToPass.put(3, "333");
        accountToPass.put(4, "444");
        accountToPass.put(5, "555");
    }

    public boolean isValid(int id, String pass) {
        if (!accountToPass.containsKey(id)) {
            return false;
        }
        if (!accountToPass.get(id).equals(pass)) {
            return false;
        }
        return true;
    }
}
