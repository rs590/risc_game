package RISC;


import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.GameMapFactory;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

import java.io.IOException;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * This class is used to store RISC.game information
 *
 * @author cirun zhang
 */
public class Game extends Thread implements Serializable {
    public final int gameID;
    public final String gameName;
    public boolean isStarted;
    protected int numPlayer;  //total number of players

    public int numActivePlayers; //todo
    protected Map<Integer, Player> idToPlayers;
    protected volatile Map<Player, Socket> playersToSockets;
    protected boolean isGameEnd; //is the game over
    protected GameMapFactory gameMapFactory; //factory of map
    protected boolean isGameAlreadyInitialized;

    private MessageReceiver messageReceiver; //message receiver
    private MessageSender messageSender; //message sender

    public CyclicBarrier allPlayerConnectedBarrier;
    protected GameMap gameMap; //the game map to play with


    /**
     * Game constructor
     *
     * @param numPlayer Number of players
     */
    public Game(int numPlayer, int gameID, String gameName) {
        this.gameID = gameID;
        this.gameName = gameName;
        this.numPlayer = numPlayer;
        this.idToPlayers = new LinkedHashMap<>();
        this.playersToSockets = new LinkedHashMap<>();
        this.messageReceiver = new MessageReceiver(numPlayer);
        this.messageSender = new MessageSender(this);
        this.isGameEnd = false;
        this.gameMapFactory = new GameMapFactory();
        this.isGameAlreadyInitialized = false;
        this.isStarted = false;

        this.allPlayerConnectedBarrier = new CyclicBarrier(numPlayer + 1);
    }

    @Override
    public void run() {
        waitAllPlayerConnect();
        this.isStarted = true;
        System.out.println("all player connected!");
        this.gameMap = gameMapFactory.createMap(numPlayer, new ArrayList<>(idToPlayers.keySet())); //2-5 player game map
        addTerritoriesToPlayers();
        waitAllPlayerRejoin();
        gameInit();
        System.out.println("game setup finished!");
        startGame();
        System.out.println("game end!");
        closeGame();
    }

    /**
     * Wait all player join the game before game start
     */
    private void waitAllPlayerConnect() {
        try {
            allPlayerConnectedBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }


    /**
     * Start the game
     *
     * 1 send map view to all players
     * 2 wait all players send back their actions
     * 3 update map
     */
    private void startGame() {
        System.out.println("RISC.game start");
        while (!this.isGameEnd) {
            waitAllPlayerRejoin();
            boolean isSendSucceed = messageSender.sendGameInfoToAllPlayers();

            Map<Integer, Message> messages = messageReceiver.getAllMessages(); //blocking method

            processAllActions(messages);
            gameMap.addOneUnitToAllTerritorries();
            updateIsGameEnd();
            updateAllPlayer();
        }

        sendGameEndInfo();
    }

    /**
     * Process all actions for all players
     *
     * @param messages The messages received
     */
    private void processAllActions(Map<Integer, Message> messages) {
        gameMap.decreaseCloack();

        for (Player player : idToPlayers.values()) {
            player.clearAllResults();
        }
        //cloacking
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.cloackingAction != null) {
                err = message.cloackingAction.execute(gameMap, player,idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //spy upgrades
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.spyUpgradeAction != null) {
                err = message.spyUpgradeAction.execute(gameMap, player,idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //spy move
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.spyMoveAction != null) {
                err = message.spyMoveAction.execute(gameMap, player,idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.upgradeUnitsAction != null) {
                err = message.upgradeUnitsAction.execute(gameMap, player, idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //process move actions
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.moveAction != null) {
                err = message.moveAction.execute(this.gameMap, player, idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //process attack consume
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.attackAction != null) {
                err = message.attackAction.consumeArmy(this.gameMap, player);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //process attack actions
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.attackAction != null) {
                err = message.attackAction.execute(this.gameMap, player, idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        for (Map.Entry<Integer, Message> entry : messages.entrySet()) { //tech level upgrade
            String err = null;
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            if (message != null && message.upgradeTechLevelAction != null && message.upgradeTechLevelAction.isActive()) {
                err = message.upgradeTechLevelAction.execute(this.gameMap, player, idToPlayers);
            }
            if (err != null) {
                System.out.println(err);
            }
        }

        //set all spies can move
        for (Player player : idToPlayers.values()) {
            player.setAllSpyAvaliableToMove();
        }

        for (Player player : idToPlayers.values()) { //add food and tech resources
            player.addResourcesFromTerritories();
        }

        for (Player player : idToPlayers.values()) { //add food and tech resources
            player.updateVisited(gameMap);
        }
    }

    /**
     * Send game end info to all players
     */
    private void sendGameEndInfo() {
        messageSender.sendGameInfoToAllPlayers();
    }

    /**
     * Update isGameEnd and check is there anybody wins
     */
    private void updateIsGameEnd() {
        List<Territory> territories = gameMap.getTerritoriesList();
        int playerId = territories.get(0).getOwnerID();

        for (int i = 1; i < territories.size(); i++) {
            if (playerId != territories.get(i).getOwnerID()) {
                return;
            }
        }
        this.isGameEnd = true;
        this.idToPlayers.get(playerId).setWin(true);
    }

    /**
     * Update players' information
     */
    private void updateAllPlayer() {
        for (Player player : idToPlayers.values()) {  //update territory list
            player.setTerritoryList(gameMap.getTerritoriesForPlayer(player.getId()));
            player.setLost(true);
        }

        for (Territory territory : gameMap.getTerritories()) {  //update isLost
            idToPlayers.get(territory.getOwnerID()).setLost(false);
        }
    }

    /**
     * Initialize game, setup units
     */
    private void gameInit() {
        messageSender.sendGameInfoToAllPlayers();
        Map<Integer, Message> messages = messageReceiver.getAllMessages(); //blocking method
        for (Map.Entry<Integer, Message> entry : messages.entrySet()) {
            Player player = idToPlayers.get(entry.getKey());
            Message message = entry.getValue();
            assert message.setUnitAction != null;
            message.setUnitAction.execute(this.gameMap, player, idToPlayers);
        }
        this.isGameAlreadyInitialized = true;
    }

    /**
     * Close the game
     */
    private void closeGame() {
        messageReceiver.close();
        messageReceiver.close();
        for (Socket socket : playersToSockets.values()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Wait all player rejoin the game room
     */
    public void waitAllPlayerRejoin() {
        System.out.println("waiting players rejoin... game paused");
        boolean waitForPlayer = true;
        while (waitForPlayer) {
            waitForPlayer = isPlayerAbsent();
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("game resumed");
    }

    /**
     * Check if any player is absent
     *
     * @return if any player is absent
     */
    private boolean isPlayerAbsent() {
        for (Map.Entry<Player, Socket> entry : playersToSockets.entrySet()) {
            Player player = entry.getKey();
            Socket socket = entry.getValue();
            if (player.isLost()) { //if the player lost, allow it to leave the game
                continue;
            }

            if (socket == null) {
                return true;
            }

            try {
                socket.sendUrgentData(0xFF);
                Thread.sleep(100);
                socket.sendUrgentData(0xFF);
                Thread.sleep(100);
                socket.sendUrgentData(0xFF);
            } catch (IOException | InterruptedException e) { //if the player is disconnected
                player.setConnected(false);
                this.playersToSockets.put(player, null); //Set the socket to null
                this.messageReceiver.removeHandler(player); //notify message receiver to remove dead handler
                return true;
            }
            System.out.println("send successfully");
        }
        return false;
    }

    /**
     * Add a new player to the game before the game start
     *
     * @param player new connected player
     * @param socket socket to communicate
     */
    public void addPlayer(Player player, Socket socket) {
        System.out.println("a new player connected!");
        synchronized (this) {
            player.setActiveGameID(this.gameID);
            this.idToPlayers.put(player.getId(), player);
            this.playersToSockets.put(player, socket);
            this.messageReceiver.addHandler(player, socket);
            this.numActivePlayers++;
            player.setLost(false);
            player.setWin(false);
        }
        System.out.println("a new player has connected to the Game");
        try {
            this.allPlayerConnectedBarrier.await();
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    /**
     * Let a player rejoin the game
     *
     * @param player Player to rejoin
     * @param socket Socket to communicate
     */
    public void rejoinPlayer(Player player, Socket socket) {
        synchronized (this) { //synchronize when multiple player connect at the same time
            this.playersToSockets.put(player, socket); //update socket
            this.messageReceiver.addHandler(player, socket);
            this.numActivePlayers++;
            updateIsGameEnd();
            updateAllPlayer();
        }

        if (!messageReceiver.isReceived(player)) { //is not yet received the message from the player, resend the
            messageSender.sendGameInfoToPlayer(player);
        }

        System.out.println("player " + player.getId() + " has rejoined the game");
    }

    /**
     * Is the player involved in this game
     *
     * @param player A player
     * @return Involved or not
     */
    public boolean isPlayerInvolved(Player player) {
        return this.playersToSockets.containsKey(player);
    }

    /**
     * Add territories to the players
     */
    private void addTerritoriesToPlayers() {
        for (Player player : this.idToPlayers.values()) {
            player.setTerritoryList(gameMap.getTerritoriesForPlayer(player.getId()));
        }
    }
}
