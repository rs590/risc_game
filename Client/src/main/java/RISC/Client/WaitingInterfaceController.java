package RISC.Client;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class WaitingInterfaceController implements Initializable {


    public ImageView background;
    public Hyperlink Scadrial;
    public Hyperlink Oz;
    public Hyperlink Roshar;
    public Hyperlink Hogwarts;
    public Hyperlink Bogo;
    public Hyperlink Narnia;
    public Hyperlink Elantris;
    public Hyperlink Midkemia;
    public Hyperlink Gondor;
    public Hyperlink Mordor;
    public Text readyInfo;
    public Button yes_button;
    public Text waitingInfo;
    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;
    public Text text5;

    private ViewAlter viewAlter;
    private int flag;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        background.setFitHeight(480);
        background.setFitWidth(640);
        setTextStyle(text1);
        setTextStyle(text2);
        setTextStyle(text3);
        setTextStyle(text4);
        setTextStyle(text5);
        flag = 0;
    }

    private void setTextStyle(Text text){
        text.setStyle(" -fx-text-fill: linear-gradient(from 0% 0% to 100% 200%, repeat, #bd9809 0%, #f8d838 50%);");
        text.setFont(Font.font("Verdana", FontPosture.ITALIC, 15));
    }

    private void setButtonType(Button button){
        button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white; -fx-font: 15 arial;");
        button.setEffect(new DropShadow());
        button.onMouseMovedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: #FFC125;-fx-font: 18 arial;-fx-border-color: green;");
                button.setEffect(null);


            }
        });
        button.onMouseExitedProperty().set(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                button.setStyle("-fx-background-color: darkslateblue; -fx-text-fill: white;-fx-font: 15 arial;");
                button.setEffect(new DropShadow());
            }
        });
    }

    public void init(){
        Task<Integer> task = new Task<>() {
            @Override
            protected Integer call() throws Exception {
                return viewAlter.waitForJoiningGame();
            }

            @Override
            protected void succeeded() {
                try {
                    if(this.getValue()==0){
                        setButtonType(yes_button);
                        waitingInfo.setVisible(false);
                        readyInfo.setVisible(true);
                        yes_button.setVisible(true);
                        flag = 1;
                    }else{
                        setButtonType(yes_button);
                        waitingInfo.setVisible(false);
                        readyInfo.setVisible(true);
                        yes_button.setVisible(true);
                        flag = 2;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        new Thread(task).start();
    }

    public void setApp(ViewAlter viewAlter) {
        this.viewAlter = viewAlter;
    }

    public void ready(ActionEvent actionEvent) throws Exception {
        if(flag==1){
            viewAlter.gotoSetUnitAction();
        } else if(flag==2) {
            viewAlter.gotoNewGame();
        }
    }

    private void showInfo(String name) throws IOException {
        URL in = BlockView.class.getResource("/View/territory_backgroundview.fxml");
        assert (in!=null);

        AnchorPane an = FXMLLoader.load(in);

        ImageView imageView = (ImageView) an.lookup("#picture");
        Label label = (Label) an.lookup("#T_name");
        label.setText(name);
        Text text = (Text) an.lookup("#text");

        setPictureText(text,imageView,name);

        Stage stage = new Stage();

        stage.initOwner(viewAlter.getPrimaryStage());

        stage.initModality(Modality.WINDOW_MODAL);

        Scene scene = new Scene(an);
        stage.setScene(scene);

        stage.setTitle(name+" Information");
        stage.show();
    }

    private void setPictureText(Text text,ImageView imageView,String name){
        if(name.equals("Narnia")){
            imageView.setImage(new Image("/View/image/Narnia.jpeg"));
            text.setText("""
                    It is said to be the first land where the ancestors of the West set foot,
                    
                    once spreading the faith of the old gods,
                    
                    and now a loose confederation of towns and villages.
                    
                    Rich, peaceful, and guarded by paladins, valor, honor,
                    
                    and virtue are prized,
                    
                    and martial arts tournaments are an annual event.
                    """);
        }
        if(name.equals("Elantris")){
            imageView.setImage(new Image("/View/image/Elantris.jpeg"));
            text.setText("""
                    For many years, a magnificent military fortress sheltered the local people.
                     
                    And the towering walls were built around it.                     
                    """);
        }
        if(name.equals("Midkemia")){
            imageView.setImage(new Image("/View/image/Midkemia.jpeg"));
            text.setText("""
                    A famous seaport city where ocean-going ships
                     
                    With back spices, gold and knowledge.
                    
                    And technology developed at a rapid pace.
                                        
                    The people here are ambitious 
                    
                    And keenly interested in discovering wealth and exploring the roots of knowledge.
                    """);
        }
        if(name.equals("Oz")){
            imageView.setImage(new Image("/View/image/Oz.jpeg"));
            text.setText("""
                    A city that thrives on jade,
                     
                    A flower of great beauty that blooms in the desert, radiant and luxurious.
                    
                    Thanks to the rich jade resources and the shelter of the jade boundary,
                    
                    Jade City has been the only city in the cloud desert
                    
                    It has made countless wealthy myths in the continuous flourishing of commerce.
                    """);
        }
        if(name.equals("Hogwarts")){
            imageView.setImage(new Image("/View/image/Hogwarts.jpeg"));
            text.setText("""
                    Close to the southeast coast
                    
                    Shipping and ports are well developed
                     
                    People here are keen on pleasure and the pursuit of wealth. 
                    
                    It hardly ever receives the impact of war.                   
                    """);
        }
        if(name.equals("Scadrial")){
            imageView.setImage(new Image("/View/image/Scadrial.png"));
            text.setText("""
                    A treacherous highland area,
                     
                    A necessary link between the East and the Western continent.
                     
                    A place that has been repeatedly contested by soldiers,
                     
                    A solid fortress has been built from the ruins of the past.                    
                    """);
        }
        if(name.equals("Roshar")){
            imageView.setImage(new Image("/View/image/Roshar.jpeg"));
            text.setText("""
                    Long inhabited by stragglers, refugees and demonic species.
                    
                    It has created order and chaos.
                    """);
        }
        if(name.equals("Mordor")){
            imageView.setImage(new Image("/View/image/Mordor.jpeg"));
            text.setText("""
                    It is an evil city.
                    
                    Countless devils lived here.
                    
                    And the evil eye looking down at all the creatures on the ground.
                    """);
        }
        if(name.equals("Gondor")){
            imageView.setImage(new Image("/View/image/Gondor.jpeg"));
            text.setText("""
                    The city of scholars, a sacred place of knowledge and collection of civilization.
                    
                    Scholars once fled the war and traveled deep into the desert, 
                    
                    carving grottoes into the mountain walls to house their knowledge and texts.                                        
                    """);
        }
        if(name.equals("Bogo")){
            imageView.setImage(new Image("/View/image/Bogo.jpeg"));
            text.setText("""
                    A wealthy frontier city 
                    
                    With a unique advantage in both agriculture and trade.
                                        
                    """);
        }
    }

    public void Scadrial_info(ActionEvent actionEvent) throws IOException {
        showInfo("Scadrial");
    }

    public void Oz_info(ActionEvent actionEvent) throws IOException {
        showInfo("Oz");
    }

    public void Roshar_info(ActionEvent actionEvent) throws IOException {
        showInfo("Roshar");
    }

    public void Hogwarts_info(ActionEvent actionEvent) throws IOException {
        showInfo("Hogwarts");
    }

    public void Bogo_info(ActionEvent actionEvent) throws IOException {
        showInfo("Bogo");
    }

    public void Narnia_info(ActionEvent actionEvent) throws IOException {
        showInfo("Narnia");
    }

    public void Elantris_info(ActionEvent actionEvent) throws IOException {
        showInfo("Elantris");
    }

    public void Midkemia_info(ActionEvent actionEvent) throws IOException {
        showInfo("Midkemia");
    }

    public void Gondor_info(ActionEvent actionEvent) throws IOException {
        showInfo("Gondor");
    }

    public void Mordor_info(ActionEvent actionEvent) throws IOException {
        showInfo("Mordor");
    }


}
