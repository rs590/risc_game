package RISC.Client.SHARE.action;



import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;
import RISC.Client.SHARE.result.AttackResult;
import RISC.Client.SHARE.result.DefendResult;
import RISC.Client.SHARE.rule.AttackReachableChecker;
import RISC.Client.SHARE.rule.EnoughFoodToAttackChecker;
import RISC.Client.SHARE.rule.EnoughUnitsToConsumeAttackChecker;
import RISC.Client.SHARE.rule.ValidTerritoryNameChecker;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class AttackAction extends Action {
    public AttackAction() {
        super();
        this.ruleChecker = new ValidTerritoryNameChecker(new AttackReachableChecker(new EnoughUnitsToConsumeAttackChecker(new EnoughFoodToAttackChecker(null))));
    }

    /**
     * Execute the attack action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        Map<Integer, Map<Territory, List<Integer>>> attackerIDToMap = new LinkedHashMap<>();
        for (int i = 0; i < from.size(); i++) { //get all attack information
            String toName = to.get(i);
            Territory toTerritory = gameMap.getTerritoryByName(toName);
            List<Integer> unitForAttack = units.get(i);
            int attackerID = player.getId();

            if (!attackerIDToMap.containsKey(attackerID)) {
                attackerIDToMap.put(attackerID, new LinkedHashMap<>());
            }
            Map<Territory, List<Integer>> attackMap = attackerIDToMap.get(attackerID);
            if (!attackMap.containsKey(toTerritory)) {
                attackMap.put(toTerritory, unitForAttack);
            } else {
                List<Integer> previousAttack = attackMap.get(toTerritory);
                for (int j = 0; j < previousAttack.size(); j++) { //add up same attack force
                    previousAttack.set(j, previousAttack.get(j) + unitForAttack.get(j));
                }
            }
        }

        for (Map.Entry<Integer, Map<Territory, List<Integer>>> entry : attackerIDToMap.entrySet()) {
            int attackerID = entry.getKey();
            Map<Territory, List<Integer>> attackMap = entry.getValue();

            for (Map.Entry<Territory, List<Integer>> attackEntry : attackMap.entrySet()) {
                Territory attackTo = attackEntry.getKey();
                int defenderID = attackTo.getOwnerID();

                List<Integer> attackerUnits = attackEntry.getValue();
                int maxLevelAttacker = getHighestBonusUnitIndex(attackerUnits);
                int maxLevelDefender = getHighestBonusUnitIndex(attackTo.getUnitList());

                //calculate results
                int foodToConsume = 0;
                int numLeft = 0;
                for (int unit : attackerUnits) {
                    foodToConsume += unit;
                }
                int defenderNum = 0;
                for(int unit:attackTo.getUnitList()){
                    defenderNum += unit;
                }
                int owner = attackTo.getOwnerID();

                boolean isWin = battle(attackerUnits, attackerID, attackTo);

                for (int unit : attackTo.getUnitList()) {
                    numLeft += unit;
                }

                //add result to result list
                AttackResult attackResult = new AttackResult(attackTo, owner, foodToConsume, maxLevelAttacker, foodToConsume, isWin ? numLeft : 0, isWin);
                DefendResult defendResult = new DefendResult(attackTo, attackerID, maxLevelDefender, numLeft, defenderNum, !isWin);

                player.results.addAResult(attackResult);
                idToPlayers.get(defenderID).results.addAResult(defendResult);
            }
        }
        return null;
    }

    // consume units before attack
    public String consumeArmy(GameMap gameMap, Player player) {
        String err = ruleChecker.isValid(gameMap, player, this);

        if (err != null) {
            return err;
        }
        for (int i = 0; i < from.size(); i++) {
            String fromName = from.get(i);
            Territory fromTerritory = gameMap.getTerritoryByName(fromName);
            List<Integer> unitToAttack = units.get(i);
            int foodToConsume = 0;
            for (int unit : unitToAttack) {
                foodToConsume += unit;
            }
            fromTerritory.decreaseUnitsNum(unitToAttack);
            player.decreaseTotalFoodResource(foodToConsume);
        }
        return null;
    }

    // attack process
    private boolean battle(List<Integer> attackerUnits, int attackerID, Territory to) {
        List<Integer> defenderUnits = to.getUnitList();
        int[] bonus = {0, 1, 3, 5, 8, 11, 15};
        boolean flag = true;

        while (!isFail(attackerUnits) && !isFail(defenderUnits)) {
            int randomAttack = ThreadLocalRandom.current().nextInt(1, 21);  // roll dice
            int randomDefend = ThreadLocalRandom.current().nextInt(1, 21);

            int attackerUnitIndex;
            int defenderUnitIndex;
            if (flag) {
                attackerUnitIndex = getHighestBonusUnitIndex(attackerUnits);
                defenderUnitIndex = getLowestBonusUnitIndex(defenderUnits);
            } else {
                attackerUnitIndex = getLowestBonusUnitIndex(attackerUnits);
                defenderUnitIndex = getHighestBonusUnitIndex(defenderUnits);
            }
            flag = !flag; //revert flag

            randomAttack += bonus[attackerUnitIndex];   // add bonus
            randomDefend += bonus[defenderUnitIndex];

            if (randomAttack > randomDefend) {
                defenderUnits.set(defenderUnitIndex, defenderUnits.get(defenderUnitIndex) - 1); //attacker win
            } else {
                attackerUnits.set(attackerUnitIndex, attackerUnits.get(attackerUnitIndex) - 1); //attacker lose
            }
        }
        boolean isWin = !isFail(attackerUnits);
        if (isWin) { //if attacker wins
            to.increaseUnitsNum(attackerUnits);
            to.setOwner(attackerID);
        }
        return isWin;
    }

    private boolean isFail(List<Integer> units) {
        for (int unit : units) {
            if (unit > 0) {
                return false;
            }
        }
        return true;
    }

    private int getHighestBonusUnitIndex(List<Integer> units) {
        for (int i = units.size() - 1; i >= 0; i--) {
            if (units.get(i) > 0) {
                return i;
            }
        }
        return 0;
    }

    private int getLowestBonusUnitIndex(List<Integer> units) {
        for (int i = 0; i < units.size(); i++) {
            if (units.get(i) > 0) {
                return i;
            }
        }
        return 0;
    }
}
