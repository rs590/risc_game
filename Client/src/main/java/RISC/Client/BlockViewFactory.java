package RISC.Client;

import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Territory;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

public class BlockViewFactory {
    GameInfo gameInfo;
    AnchorPane mapAp;
    List<BlockView> blocks;

    public BlockViewFactory(GameInfo gameInfo, AnchorPane mapAp) {
        this.gameInfo = gameInfo;
        this.mapAp = mapAp;
        blocks = new ArrayList<>();
    }

    public void createBlockView(Territory territory, Rectangle rectangle,Text text){
        BlockView blockView = new BlockView(territory,rectangle,text,gameInfo.player);
        blocks.add(blockView);
        rectangle.setStroke(Color.BLACK);
        rectangle.setStrokeWidth(3);
        if(territory.isVisible(gameInfo.player.getId())){
            rectangle.setFill(BlockView.colors.get(territory.getOwnerID()));
        } else{
            rectangle.setFill(BlockView.colors.get(5));
        }
        rectangle.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setStroke(Color.RED);
            }
        });
        rectangle.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setStroke(Color.BLACK);
            }
        });
        text.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                rectangle.setStroke(Color.RED);
            }
        });
        mapAp.getChildren().add(rectangle);
        mapAp.getChildren().add(text);
    }

    public List<BlockView> createMap(){
        createNarnia();
        createElantris();
        createMidkemia();
        createScadrial();
        createRoshar();
        createOz();
        createMordor();
        createGondor();
        if(gameInfo.playerNum==3){
            createHogwarts();
        }
        if(gameInfo.playerNum==5){
            createHogwarts();
            createBogo();
        }
        return blocks;
    }

    public void createNarnia(){
        Territory territory = gameInfo.map.getTerritoryByName("Narnia");
        Rectangle rectangle = new Rectangle(60.0,65.0,50.0,50.0);
        String name = territory.getName();
        double x = rectangle.getX()+5;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createElantris(){
        Territory territory = gameInfo.map.getTerritoryByName("Elantris");
        Rectangle rectangle = new Rectangle(50.0,118.0,80.0,80.0);
        String name = territory.getName();
        double x = rectangle.getX()+20;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createMidkemia(){
        Territory territory = gameInfo.map.getTerritoryByName("Midkemia");
        Rectangle rectangle = new Rectangle(113.0,50.0,80.0,65.0);
        String name = territory.getName();
        double x = rectangle.getX()+10;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createScadrial(){
        Territory territory = gameInfo.map.getTerritoryByName("Scadrial");
        Rectangle rectangle = new Rectangle(133.0,118.0,150.0,50.0);
        String name = territory.getName();
        double x = rectangle.getX()+50;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createRoshar(){
        Territory territory = gameInfo.map.getTerritoryByName("Roshar");
        Rectangle rectangle = new Rectangle(133.0,171.0,150.0,50.0);
        String name = territory.getName();
        double x = rectangle.getX()+50;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createOz(){
        Territory territory = gameInfo.map.getTerritoryByName("Oz");
        Rectangle rectangle = new Rectangle(196.0,30.0,50.0,85.0);
        String name = territory.getName();
        double x = rectangle.getX()+15;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createMordor(){
        Territory territory = gameInfo.map.getTerritoryByName("Mordor");
        Rectangle rectangle = new Rectangle(249.0,70,70.0,45.0);
        String name = territory.getName();
        double x = rectangle.getX()+15;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createGondor(){
        Territory territory = gameInfo.map.getTerritoryByName("Gondor");
        Rectangle rectangle = new Rectangle(249.0,37,130.0,30.0);
        String name = territory.getName();
        double x = rectangle.getX()+40;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        createBlockView(territory,rectangle,text);
    }

    public void createHogwarts(){
        Territory territory = gameInfo.map.getTerritoryByName("Hogwarts");
        Rectangle rectangle = new Rectangle(286.0,118,33.0,120.0);
        String name = territory.getName();
        double x = rectangle.getX()-10;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        text.setRotate(90);
        createBlockView(territory,rectangle,text);
    }

    public void createBogo(){
        Territory territory = gameInfo.map.getTerritoryByName("Bogo");
        Rectangle rectangle = new Rectangle(322.0,70,55.0,80.0);
        String name = territory.getName();
        double x = rectangle.getX()+10;
        double y = rectangle.getY()+rectangle.getHeight()/2;
        Text text = new Text(x,y,name);
        text.setRotate(90);
        createBlockView(territory,rectangle,text);
    }
}
