package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MoveActionTest {
    @Test
    public void test_move() {
        MoveAction moveAction = new MoveAction();
        assertThrows(NullPointerException.class, ()-> moveAction.execute(null, null, null));
    }

}