package RISC;

import RISC.Client.SHARE.action.ChooseGameRoomInfo;
import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.utility.SerializeUtility;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlayerHandler extends Thread {
    private Socket socket; //socket to communicate
    private volatile Map<Integer, Player> allGlobalPlayers; //all players on the server
    private volatile Map<Integer, Game> allGlobalGames; //all games on the server
    private UserDB userDB; //user database
    SerializeUtility utility;

    public PlayerHandler(Socket socket, Map<Integer, Player> allGlobalPlayers, Map<Integer, Game> allGlobalGames, UserDB userDB) {
        super();
        this.socket = socket;
        this.allGlobalPlayers = allGlobalPlayers;
        this.allGlobalGames = allGlobalGames;
        this.userDB = userDB;
        this.utility = new SerializeUtility();
    }

    @Override
    public void run() {
        try {
            createNewPlayer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a new player and push it to the game
     *
     * @throws IOException May throw IOException
     */
    private void createNewPlayer() throws IOException {
        Player player = userLogin(socket);
        if (player == null) { //player refused to log in
            return;
        }
        Game activeGame = whichGameToConnect(player, socket); //todo this is a demo

        if (activeGame == null) {
            return;
        }

        player.setActiveGameID(activeGame.gameID);
        System.out.println("doing my job");

        if (!activeGame.playersToSockets.containsKey(player)) {
            activeGame.addPlayer(player, socket);
        } else {
            activeGame.rejoinPlayer(player, socket);
        }

        System.out.println("done my job");
    }

    /***
     * Let user to login
     *
     * @param socket Socket to communicate
     * @return The logged in player
     * @throws IOException May throw IOException
     */
    private Player userLogin(Socket socket) throws IOException {
        int playerID;
        Player newPlayer;

        while (true) {
            utility.sendGameInfo(new GameInfo(false), socket.getOutputStream());
            Message message = utility.recvMessage(socket.getInputStream());

            if (message == null) {
                System.out.println("user does not want to login now");
                return null;
            }

            playerID = message.loginAction.playerID;
            String password = message.loginAction.password;
            if (!userDB.isValid(playerID, password)) {
                continue;
            }

            if (allGlobalPlayers.containsKey(playerID)) { //if the player ever logged in
                newPlayer = allGlobalPlayers.get(playerID);
            } else {
                newPlayer = new Player(playerID);
                allGlobalPlayers.put(playerID, newPlayer);
            }

            if (newPlayer.isConnected()) {
                System.out.println("Player " + playerID + " already logged in!");
                continue;
            }
            break;

        }

        try {
            utility.sendGameInfo(new GameInfo(true), socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Player " + playerID + " connected with correct password");
        newPlayer.setConnected(true);
        return newPlayer;
    }

    /**
     * Let user choose which game to connect
     *
     * @param player Player
     * @param socket Socket of player
     * @return Game to play
     * @throws IOException May throw IOException
     */
    private Game whichGameToConnect(Player player, Socket socket) throws IOException {
        boolean chooseRoomSuccess = false;
        String errMessage = null;
        Game gameToConnect = null;
        while (!chooseRoomSuccess) {
            utility.sendChooseGameRoomInfo(createChooseGameRoomInfo(chooseRoomSuccess, errMessage), socket.getOutputStream());

            Message message = utility.recvMessage(socket.getInputStream());

            if (message == null) {
                System.out.println("user does not want to choose game now");
                player.setConnected(false);
                return null;
            }

            int gameID = message.chooseGameAction.gameID;
            if (!allGlobalGames.containsKey(gameID)) {
                errMessage = "err: cannot found game id = " + gameID;
                continue;
            }

            gameToConnect = allGlobalGames.get(gameID);

            if (gameToConnect.isStarted && !gameToConnect.isPlayerInvolved(player)) {
                errMessage = "err: game already started,  you are not belong to this game";
                continue;
            }
            chooseRoomSuccess = true;
        }

        utility.sendChooseGameRoomInfo(createChooseGameRoomInfo(true, null), socket.getOutputStream());

        return gameToConnect;
    }

    /**
     * Create ChooseGameRoomInfo object
     *
     * @param chooseRoomSuccess is chosen game success
     * @param errMessage        err to print
     * @return ChooseGameRoomInfo object
     */
    private ChooseGameRoomInfo createChooseGameRoomInfo(boolean chooseRoomSuccess, String errMessage) {
        List<Integer> gameIDs = new ArrayList<>();
        List<String> gameNames = new ArrayList<>();
        List<Boolean> gameStarted = new ArrayList<>();

        for (Game game : allGlobalGames.values()) {
            gameIDs.add(game.gameID);
            gameNames.add(game.gameName);
            gameStarted.add(game.isStarted);
        }
        return new ChooseGameRoomInfo(chooseRoomSuccess, errMessage, gameIDs, gameNames, gameStarted);
    }

}
