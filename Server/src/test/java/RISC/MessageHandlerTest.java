package RISC;

import RISC.Client.SHARE.game.Player;
import org.junit.jupiter.api.Test;

import java.net.Socket;
import java.util.LinkedHashSet;
import java.util.concurrent.CyclicBarrier;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MessageHandlerTest {
    @Test
    public void test_messageHandler() {
        assertThrows(Exception.class, () -> new MessageHandler(new Player(0, new LinkedHashSet<>()), new Socket(), new MessageReceiver(0), new CyclicBarrier(0)));
    }

}