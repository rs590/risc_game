package RISC.Client.SHARE.rule;



import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;

import java.io.Serializable;

public abstract class RuleChecker implements Serializable {
    public RuleChecker nextRule; //next checker

    public RuleChecker(RuleChecker nextRule) {
        this.nextRule = nextRule;
    }

    /**
     * Check the action is valid or not
     *
     * @param gameMap game map
     * @param action  action
     * @return valid or not
     */
    protected abstract String checkMyRule(GameMap gameMap, Player player, Action action);

    /**
     * Check the action is valid or not for all checkers
     *
     * @param gameMap game map
     * @param action  action
     * @return valid or not
     */

    public String isValid(GameMap gameMap, Player player, Action action) {
        String err = checkMyRule(gameMap, player, action);
        if (err != null) {
            return err;
        }
        return nextRule == null ? null : nextRule.isValid(gameMap, player, action);
    }
}
