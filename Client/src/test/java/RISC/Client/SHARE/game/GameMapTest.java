package RISC.Client.SHARE.game;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GameMapTest {

    static GameMapFactory factory = new GameMapFactory();

    ArrayList<Integer> createPlayerList(int playerNum) {
        ArrayList<Integer> playerIDs = new ArrayList<>();
        playerIDs.add(0);
        playerIDs.add(1);
        switch (playerNum) {
            case 5:
                playerIDs.add(4);
            case 4:
                playerIDs.add(3);
            case 3:
                playerIDs.add(2);
                break;
        }
        return playerIDs;
    }

    @Test
    public void test_gameMap() {
        // Generate Game Maps
        GameMap gameMap_2 = factory.createMap(2, createPlayerList(2));
        GameMap gameMap_3 = factory.createMap(3, createPlayerList(3));
        GameMap gameMap_4 = factory.createMap(4, createPlayerList(4));
        GameMap gameMap_5 = factory.createMap(5, createPlayerList(5));

        assertNotNull(gameMap_2.getTerritoryByName("Narnia"));
        assertNull(gameMap_2.getTerritoryByName("Test"));

        gameMap_2.decreaseCloack();

        assertNotNull(gameMap_2.getTerritories());
        assertNotNull(gameMap_2.getTerritoriesList());
        assertNotNull(gameMap_2.getTerritoriesForPlayer(0));

        gameMap_2.addOneUnitToAllTerritorries();

        assertFalse(gameMap_2.isReachableForAttack(gameMap_2.getTerritoryByName("Narnia"),
                gameMap_2.getTerritoryByName("Midkemia")));

        assertTrue(gameMap_2.isReachableForAttack(gameMap_2.getTerritoryByName("Narnia"),
                gameMap_2.getTerritoryByName("Elantris")));

        assertTrue(gameMap_2.isReachableForMove(gameMap_2.getTerritoryByName("Narnia"),
                gameMap_2.getTerritoryByName("Midkemia")));

        assertFalse(gameMap_2.isReachableForMove(gameMap_2.getTerritoryByName("Narnia"),
                gameMap_2.getTerritoryByName("Elantris")));

        assertEquals(4, gameMap_2.minimalCostPath(gameMap_2.getTerritoryByName("Narnia"),
                gameMap_2.getTerritoryByName("Midkemia")));
    }
}