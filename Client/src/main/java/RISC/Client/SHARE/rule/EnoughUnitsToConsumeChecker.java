package RISC.Client.SHARE.rule;



import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

import java.util.List;

/**
 * Check if unit is enough to consume
 */
public class EnoughUnitsToConsumeChecker extends RuleChecker {

    public EnoughUnitsToConsumeChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        for (int i = 0; i < action.from.size(); i++) {
            String fromName = action.from.get(i);
            Territory fromTerritory = gameMap.getTerritoryByName(fromName);
            List<Integer> unitToConsumeList = action.units.get(i);
            List<Integer> unitList = fromTerritory.getUnitList();
            for (int level = 0; level < unitToConsumeList.size(); ++level) {
                if (unitToConsumeList.get(level) > unitList.get(level)) {
                    return "err: not enough unit nums in " + fromName + " with level: " + level;
                }
            }
        }
        return null;
    }
}
