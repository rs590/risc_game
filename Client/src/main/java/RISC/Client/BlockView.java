package RISC.Client;

import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlockView{

    Territory territory;
    Rectangle rectangle;
    Player player;
    Text name;

    static List<Color> colors = Arrays.asList(Color.SKYBLUE,Color.LIME,Color.SILVER,Color.ORANGE,Color.PINK,Color.GRAY);


    public BlockView(Territory territory, Rectangle rectangle,Text text,Player player) {
        this.territory = territory;
        this.rectangle = rectangle;
        this.name = text;
        this.player = player;
    }

    public void showTerritoryInfo(Stage Primarystage) throws IOException {
        URL in = BlockView.class.getResource("/View/territory_infoview.fxml");
        assert (in!=null);

        AnchorPane an = FXMLLoader.load(in);

        setView(an);
        setPicture(an);

        Stage stage = new Stage();

        stage.initOwner(Primarystage);

        stage.initModality(Modality.WINDOW_MODAL);

        Scene scene = new Scene(an);
        stage.setScene(scene);

        stage.setTitle(territory.getName()+" Information");
        stage.show();
    }

    private void setView(AnchorPane an){
        Label tname = (Label) an.lookup("#T_name");
        Label pname = (Label) an.lookup("#p_name");
        Label size = (Label) an.lookup("#size");
        Label food = (Label) an.lookup("#food");
        Label tech = (Label) an.lookup("#tech");
        Label unit0 = (Label) an.lookup("#u_level0");
        Label unit1 = (Label) an.lookup("#u_level1");
        Label unit2 = (Label) an.lookup("#u_level2");
        Label unit3 = (Label) an.lookup("#u_level3");
        Label unit4 = (Label) an.lookup("#u_level4");
        Label unit5 = (Label) an.lookup("#u_level5");
        Label unit6 = (Label) an.lookup("#u_levelMax");
        Label spy = (Label) an.lookup("#spy_num");
        Label neighbours = (Label) an.lookup("#neighbours");
        Rectangle color = (Rectangle) an.lookup("#player_color");
        if(territory.isVisible(player.getId())){
            tname.setText(territory.getName());
            pname.setText(String.valueOf(territory.getOwnerID()));
            size.setText(String.valueOf(territory.getSize()));
            food.setText(String.valueOf(territory.getFoodProduction()));
            tech.setText(String.valueOf(territory.getTechProduction()));
            unit0.setText(String.valueOf(territory.getUnitList().get(0)));
            unit1.setText(String.valueOf(territory.getUnitList().get(1)));
            unit2.setText(String.valueOf(territory.getUnitList().get(2)));
            unit3.setText(String.valueOf(territory.getUnitList().get(3)));
            unit4.setText(String.valueOf(territory.getUnitList().get(4)));
            unit5.setText(String.valueOf(territory.getUnitList().get(5)));
            unit6.setText(String.valueOf(territory.getUnitList().get(6)));
            spy.setText(String.valueOf(player.getSpyByTerritory(territory)));
            StringBuilder sb = new StringBuilder();
            for(Territory t:territory.getNeighbors()){
                sb.append(t.getName()+" ");
            }
            neighbours.setText(sb.toString());
            color.setFill(colors.get(territory.getOwnerID()));
        } else if(player.getVisited().contains(territory)){
            tname.setText(territory.getName());
            pname.setText("???");
            size.setText(String.valueOf(territory.getSize()));
            food.setText(String.valueOf(territory.getFoodProduction()));
            tech.setText(String.valueOf(territory.getTechProduction()));
            unit0.setText("???");
            unit1.setText("???");
            unit2.setText("???");
            unit3.setText("???");
            unit4.setText("???");
            unit5.setText("???");
            unit6.setText("???");
            StringBuilder sb = new StringBuilder();
            for(Territory t:territory.getNeighbors()){
                sb.append(t.getName()+" ");
            }
            neighbours.setText(sb.toString());
            spy.setText("0");
            color.setFill(colors.get(5));
        } else{
            tname.setText(territory.getName());
            pname.setText("???");
            size.setText("???");
            food.setText("???");
            tech.setText("???");
            unit0.setText("???");
            unit1.setText("???");
            unit2.setText("???");
            unit3.setText("???");
            unit4.setText("???");
            unit5.setText("???");
            unit6.setText("???");
            neighbours.setText("???");
            spy.setText("0");
            color.setFill(colors.get(5));
        }

    }

    private void setPicture(AnchorPane an){
        ImageView imageView = (ImageView) an.lookup("#picture");
        if(territory.getName().equals("Narnia")){
            imageView.setImage(new Image("/View/image/Narnia.jpeg"));
        }
        if(territory.getName().equals("Elantris")){
            imageView.setImage(new Image("/View/image/Elantris.jpeg"));
        }
        if(territory.getName().equals("Midkemia")){
            imageView.setImage(new Image("/View/image/Midkemia.jpeg"));
        }
        if(territory.getName().equals("Oz")){
            imageView.setImage(new Image("/View/image/Oz.jpeg"));
        }
        if(territory.getName().equals("Hogwarts")){
            imageView.setImage(new Image("/View/image/Hogwarts.jpeg"));
        }
        if(territory.getName().equals("Scadrial")){
            imageView.setImage(new Image("/View/image/Scadrial.png"));
        }
        if(territory.getName().equals("Roshar")){
            imageView.setImage(new Image("/View/image/Roshar.jpeg"));
        }
        if(territory.getName().equals("Mordor")){
            imageView.setImage(new Image("/View/image/Mordor.jpeg"));
        }
        if(territory.getName().equals("Gondor")){
            imageView.setImage(new Image("/View/image/Gondor.jpeg"));
        }
        if(territory.getName().equals("Bogo")){
            imageView.setImage(new Image("/View/image/Bogo.jpeg"));
        }
    }

}
