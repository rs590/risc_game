package RISC.Client.SHARE.rule;


import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Check if unit is enough to consume
 */
public class EnoughUnitsToConsumeMoveChecker extends RuleChecker {

    public EnoughUnitsToConsumeMoveChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        Map<String, List<Integer>> territoryNameToUnits = new HashMap<>();

        for (Territory territory : gameMap.getTerritories()) {
            territoryNameToUnits.put(territory.getName(), new ArrayList<>(territory.getUnitList()));
        }

        for (int i = 0; i < action.from.size(); i++) {
            String fromName = action.from.get(i);
            String toName = action.to.get(i);
            List<Integer> unitToConsumeList = action.units.get(i);
            List<Integer> unitFromList = territoryNameToUnits.get(fromName);
            List<Integer> unitToList = territoryNameToUnits.get(toName);
            for (int level = 0; level < unitToConsumeList.size(); ++level) {
                if (unitToConsumeList.get(level) > unitFromList.get(level)) {
                    return "err: not enough unit nums in " + fromName + " with level: " + level + ", need " + unitToConsumeList.get(level) + " but only have " + unitFromList.get(level);
                }
                unitFromList.set(level, unitFromList.get(level) - unitToConsumeList.get(level));
                unitToList.set(level, unitToList.get(level) + unitToConsumeList.get(level));
            }
        }
        return null;
    }
}
