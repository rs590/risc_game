package RISC.Client.SHARE.action;


import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.result.UpgradeResult;
import RISC.Client.SHARE.rule.EnoughTechToUpgradeUnitChecker;
import RISC.Client.SHARE.rule.ValidTerritoryNameChecker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class UpgradeUnitsAction extends Action {

    public UpgradeUnitsAction() {
        super();
        this.ruleChecker = new ValidTerritoryNameChecker(new EnoughTechToUpgradeUnitChecker(null));
        this.updateUnitCostList = new ArrayList<>(Arrays.asList(3, 8, 19, 25, 35, 50)); // cost of upgrade
    }

    /**
     * Execute the upgrade units level action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    @Override
    public String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers) {
        String err = ruleChecker.isValid(gameMap, player, this);

        if (err != null) {
            return err;
        }

        for (int i = 0; i < from.size(); ++i) {
            String updateWhere = this.from.get(i);
            int updateUnitFromLevel = this.updateUnitFromLevel.get(i);
            int updateUnitToLevel = this.updateUnitToLevel.get(i);
            int updateUnitNum = this.updateUnitNum.get(i);

            // calculate total tech needed
            int upgradeCost = 0;
            for (int j = updateUnitFromLevel; j < updateUnitToLevel; ++j) {
                upgradeCost += (updateUnitNum * this.updateUnitCostList.get(j));
            }

            UpgradeResult upgradeResult = new UpgradeResult(gameMap.getTerritoryByName(updateWhere), updateUnitFromLevel, updateUnitToLevel, updateUnitNum, upgradeCost);
            player.results.addAResult(upgradeResult);

            System.out.println("upgradeCost " + upgradeCost);
            System.out.println("updateUnitNum " + updateUnitNum);
            player.decreaseTotalTechResource(upgradeCost);
            gameMap.getTerritoryByName(updateWhere).updateUnitList(-updateUnitNum, updateUnitFromLevel);
            gameMap.getTerritoryByName(updateWhere).updateUnitList(+updateUnitNum, updateUnitToLevel);
        }

        return null;
    }

    public void addOneAction(String from, int unitLevelFrom, int unitLevelTo, int num) {
        this.from.add(from);
        this.updateUnitFromLevel.add(unitLevelFrom);
        this.updateUnitToLevel.add(unitLevelTo);
        this.updateUnitNum.add(num);
    }
}
