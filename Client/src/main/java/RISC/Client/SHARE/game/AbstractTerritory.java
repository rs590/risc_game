package RISC.Client.SHARE.game;

public interface AbstractTerritory {
    /**
     * Add neighbor to territory
     *
     * @param collection is the neighbor to add
     */
    void addNeighbors(Territory... collection);

    /**
     * Add a new neighbor
     *
     * @param territory neighbor territory
     */
    void addNeighbor(Territory territory);

    /**
     * Set owner's id
     *
     * @param ownerID id
     */
    void setOwner(int ownerID);

    /**
     * Get territory name
     *
     * @return name
     */
    String getName();

    /**
     * Get owner id
     *
     * @return id
     */
    int getOwnerID();

    /**
     * Get unit number
     *
     * @return unit number
     */
    int getUnitsNum();

    /**
     * Set unit number
     *
     * @param unitsNum unit number
     */
    void setUnitsNum(int unitsNum);
}
