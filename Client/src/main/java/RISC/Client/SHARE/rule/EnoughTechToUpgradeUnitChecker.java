package RISC.Client.SHARE.rule;



import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;

import java.util.List;

public class EnoughTechToUpgradeUnitChecker extends RuleChecker {

    public EnoughTechToUpgradeUnitChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    /**
     * Check the action is valid or not
     *
     * @param gameMap game map
     * @param player
     * @param action  action
     * @return valid or not
     */
    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        // assume level and num both correct
        // assume 1 <= level <= 6
        // example: from leve 1, num 10, upgrade to level 2, current tech level 2
        for (int i = 0; i < action.from.size(); ++i) {
            String updateWhere = action.from.get(i);
            int updateUnitFromLevel = action.updateUnitFromLevel.get(i);
            int updateUnitToLevel = action.updateUnitToLevel.get(i);
            int updateUnitNum = action.updateUnitNum.get(i);
            int totalTechResource = player.getTotalTechResource();
            int techLevel = player.getTechLevel();
            List<Integer> unitList = gameMap.getTerritoryByName(updateWhere).getUnitList();

            // False: 要升级的等级 > 当前科技等级
            if (updateUnitToLevel > techLevel) {
                return "err: " + "can not upgrade unit to level " + updateUnitToLevel + " your tech level is " + techLevel;
            }

            // False: 升级兵的数量 > 当前兵的数量
            if (updateUnitNum > unitList.get(updateUnitFromLevel)) {
                return "err: level " + unitList.get(updateUnitFromLevel) + " units is not enough";
            }

            // calculate total tech needed
            int upgradeCost = 0;
            for (int j = updateUnitFromLevel; j < updateUnitToLevel; ++j) {
                upgradeCost += (updateUnitNum * action.updateUnitCostList.get(j));
            }
            // False: 升级所需tech > 当前tech
            if (upgradeCost > totalTechResource) {
                return "err: need " + upgradeCost + "tech resources, but got " + totalTechResource;
            }

        }

        return null;
    }
}
