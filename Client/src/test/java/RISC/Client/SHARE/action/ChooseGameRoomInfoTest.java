package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class ChooseGameRoomInfoTest {
    @Test
    public void test_chooseGameRoomInfo() {
        ChooseGameRoomInfo chooseGameRoomInfo = new ChooseGameRoomInfo(true, null, null, null, null);
        assertThrows(NullPointerException.class, chooseGameRoomInfo::toString);
    }

}