package RISC.Client;

import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.*;
import RISC.Client.SHARE.result.AttackResult;
import RISC.Client.SHARE.result.DefendResult;
import RISC.Client.SHARE.result.MoveResult;
import RISC.Client.SHARE.result.UpgradeResult;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ViewAlter{

    private Stage stage;

    private GameInfo gameInfo;

    Message message;

    Client client;

//    public void init(){
//        List<Integer> ids = new ArrayList<>();
//        ids.add(0);
//        ids.add(1);
//        ids.add(2);
//        ids.add(3);
//        ids.add(4);
//        GameMap map = new GameMapFactory().createMap(5,ids);
//        Player player0 = new Player(0,map.getTerritoriesForPlayer(0));
//        Player player1 = new Player(1,map.getTerritoriesForPlayer(1));
//        Player player2 = new Player(2,map.getTerritoriesForPlayer(2));
//        Player player3 = new Player(3,map.getTerritoriesForPlayer(3));
//        Player player4 = new Player(4,map.getTerritoriesForPlayer(4));
//        List<Player> playerList = new ArrayList<>();
//        playerList.add(player0);
//        playerList.add(player1);
//        playerList.add(player2);
//        playerList.add(player3);
//        playerList.add(player4);
//        boolean isGameEnd = false;
//        gameInfo = new GameInfo(map,player0,playerList,isGameEnd,5,30,false);
//        for(Territory territory:gameInfo.map.getTerritories()){
//            territory.increaseUnitsNum(0,10);
//            territory.increaseUnitsNum(2,10);
//        }
//        MoveResult moveResult = new MoveResult(gameInfo.map.getTerritoryByName("Narnia"),
//                gameInfo.map.getTerritoryByName("Elantris"),10,0,5);
//        AttackResult attackResult1 = new AttackResult(gameInfo.map.getTerritoryByName("Narnia"),
//                gameInfo.map.getTerritoryByName("Elantris"),3,10,3,5,7,true);
//        AttackResult attackResult2 = new AttackResult(gameInfo.map.getTerritoryByName("Narnia"),
//                gameInfo.map.getTerritoryByName("Elantris"),3,10,3,5,7,false);
//        UpgradeResult upgradeResult = new UpgradeResult(gameInfo.map.getTerritoryByName("Narnia"),
//                0,4,5,10);
//        DefendResult defendResult = new DefendResult(gameInfo.map.getTerritoryByName("Elantris"),
//                gameInfo.map.getTerritoryByName("Narnia"),2,5,3,5,false);
//        gameInfo.resuts.list_move.add(moveResult);
//        gameInfo.resuts.list_upgrade.add(upgradeResult);
//        gameInfo.resuts.list_attack.add(attackResult1);
//        gameInfo.resuts.list_attack.add(attackResult2);
//        gameInfo.resuts.list_defend.add(defendResult);
//    }

    public ViewAlter(Stage stage) {
        this.stage = stage;
    }

    public Stage getPrimaryStage(){
        return this.stage;
    }

    void gotoNewGame() throws Exception {
        NewGameController gameController = (NewGameController) replaceSceneContent("/View/new_gameview.fxml");
        gameController.setGameInfo(gameInfo);
        gameController.createMap();
        gameController.setMessage(new Message());
        gameController.setApp(this);
        gameController.init();
    }

    public void gotoLogin() throws Exception {
        InterfaceController interfaceController = (InterfaceController) replaceSceneContent("/View/host_interface.fxml");
        interfaceController.setLogin(this);
    }

    public void gotoChoiceGame() throws Exception {
        InterfaceController interfaceController = (InterfaceController) replaceSceneContent("/View/chose_games.fxml");
        interfaceController.setApp(this);
    }

    public void gotoSetUnitAction() throws Exception {
        SetUnitsController setUnits = (SetUnitsController) replaceSceneContent("/View/SetUnitsView.fxml");
        setUnits.setApp(this);
        setUnits.setGameInfo(gameInfo);
        setUnits.setMap();
        setUnits.init();
    }

    public void gotoGame() throws Exception {
        NewGameController gameController = (NewGameController) replaceSceneContent("/View/new_gameView.fxml");
        gameController.setApp(this);
        gameController.setGameInfo(gameInfo);
        //gameController.setMap();
        gameController.setMessage(new Message());
        gameController.init();
    }

    public void gotoWaitInterface() throws Exception{
        WaitingInterfaceController waitingInterfaceController =
                (WaitingInterfaceController) replaceSceneContent("/View/WaitingView.fxml");
        waitingInterfaceController.setApp(this);
        waitingInterfaceController.init();
    }

    public void gotoActionController() throws Exception{
        ActionController actionController = (ActionController) replaceSceneContent("/View/ActionView.fxml");
        actionController.setApp(this);
        actionController.setGamInfo(gameInfo);
        actionController.init();
        actionController.getNextInfo();
    }

    public void waitForUpdate(Message message) throws Exception {
        this.message = message;
        this.gameInfo = client.changeMessage(message);
    }

    public int waitForJoiningGame() throws IOException {
        this.gameInfo = client.gameInit();
        if(gameInfo.isGameAlreadyInitialized){
            return 1;
        }
        return 0;
    }

    public int waitForLogIn(Message message) throws Exception {
        client = Client.getInstance();
        if(client.userLogin(message)){
            return 0;
        }
        return 1;
    }

    public int waitForChooseGame(Message message) throws Exception {
        if(client.userSelectGame(message)){
            return 0;
        }
        return 1;
    }

    /**
     * @param fxml
     */
    private Initializable replaceSceneContent(String fxml) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        InputStream in = ViewAlter.class.getResourceAsStream(fxml);
        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(ViewAlter.class.getResource(fxml));
        try {
            AnchorPane page = (AnchorPane) loader.load(in);
            Scene scene = new Scene(page, 640, 480);
            stage.setScene(scene);
            stage.sizeToScene();
        } finally {
            assert in != null;
            in.close();
        }
        return (Initializable) loader.getController();
    }

    public void closeGame() throws Exception {
        client.socket.close();
        this.gotoLogin();
    }
}
