package RISC.Client.SHARE.action;

import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.GameMapFactory;
import RISC.Client.SHARE.game.Player;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNull;

class AttackActionTest {

    static GameMapFactory factory = new GameMapFactory();

    ArrayList<Integer> createPlayerList(int playerNum) {
        ArrayList<Integer> playerIDs = new ArrayList<>();
        playerIDs.add(0);
        playerIDs.add(1);
        switch (playerNum) {
            case 5:
                playerIDs.add(4);
            case 4:
                playerIDs.add(3);
            case 3:
                playerIDs.add(2);
                break;
        }
        return playerIDs;
    }

    @Test
    public void test_attack() {
        GameMap map = factory.createMap(2, createPlayerList(2));
        Player player_0 = new Player(0, map.getTerritoriesForPlayer(0));
        AttackAction attackAction = new AttackAction();
        assertNull(attackAction.execute(map, player_0, null));
        assertNull(attackAction.consumeArmy(map, player_0));
    }

}