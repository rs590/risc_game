package RISC.Client.SHARE.action;

import java.io.Serializable;
import java.util.List;

public class ChooseGameRoomInfo implements Serializable {
    private boolean chooseRoomSuccess;
    private String errMessage;
    private List<Integer> gameIDs;
    private List<String> gameNames;
    private List<Boolean> gameStarted;

    public ChooseGameRoomInfo(boolean chooseRoomSuccess, String errMessage, List<Integer> gameIDs, List<String> gameNames, List<Boolean> gameStarted) {
        this.chooseRoomSuccess = chooseRoomSuccess;
        this.gameIDs = gameIDs;
        this.gameNames = gameNames;
        this.gameStarted = gameStarted;
        this.errMessage = errMessage;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < gameIDs.size(); i++) {
            sb.append("game id: ").append(gameIDs.get(i));
            sb.append(" -- ");
            sb.append(gameNames.get(i));
            sb.append(" -- alerady started: ").append(gameStarted.get(i));
            sb.append("\n");
        }
        return sb.toString();
    }

    public boolean isChooseRoomSuccess() {
        return chooseRoomSuccess;
    }

    public String getErrMessage() {
        return errMessage;
    }

    public List<Integer> getGameIDs() {
        return gameIDs;
    }

    public List<String> getGameNames() {
        return gameNames;
    }

    public List<Boolean> getGameStarted() {
        return gameStarted;
    }
}
