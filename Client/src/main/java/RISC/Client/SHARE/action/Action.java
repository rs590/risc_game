package RISC.Client.SHARE.action;


import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.rule.RuleChecker;

import java.io.Serializable;
import java.util.*;

/**
 * This class is the abstract action for all others
 *
 * @author cirun zhang
 */
public abstract class Action implements Serializable {

    public List<String> from;  //source territories
    public List<String> to;    //dest territories
    public List<List<Integer>> units; //number of units
    public RuleChecker ruleChecker; //rule checker

    public List<Integer> updateUnitFromLevel;
    public List<Integer> updateUnitToLevel;
    public List<Integer> updateUnitNum;
    public List<Integer> updateUnitCostList;

    public List<Integer> updateTechCostList;

    public Action() {
        this.from = new ArrayList<>();
        this.to = new ArrayList<>();
        this.units = new ArrayList<>();
        this.updateUnitFromLevel = new ArrayList<>();
        this.updateUnitToLevel = new ArrayList<>();
        this.updateUnitNum = new ArrayList<>();
    }

    public Action(List<String> from, List<String> to, List<List<Integer>> units) {
        this.from = from;
        this.to = to;
        this.units = units;
    }

    public Action(List<String> from, List<Integer> updateUnitFromLevel, List<Integer> updateUnitToLevel, List<Integer> updateUnitNum) {
        this.from = from;
        this.updateUnitFromLevel = updateUnitFromLevel;
        this.updateUnitToLevel = updateUnitToLevel;
        this.updateUnitNum = updateUnitNum;
    }

    public void addOneAction(String f, String t, int unitLevel, int unitNum) {
        this.from.add(f);
        this.to.add(t);
        Integer[] temp = new Integer[7];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = 0;
        }

        temp[unitLevel] = unitNum;
        this.units.add(Arrays.asList(temp));
    }

    /**
     * Execute the action
     *
     * @param gameMap map to modify
     * @param player  player of the action
     * @return error message or null
     */
    public abstract String execute(GameMap gameMap, Player player, Map<Integer, Player> idToPlayers);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action = (Action) o;

        if (!Objects.equals(from, action.from)) return false;
        if (!Objects.equals(to, action.to)) return false;
        return Objects.equals(units, action.units);
    }

    @Override
    public int hashCode() {
        int result = from != null ? from.hashCode() : 0;
        result = 31 * result + (to != null ? to.hashCode() : 0);
        result = 31 * result + (units != null ? units.hashCode() : 0);
        return result;
    }
}
