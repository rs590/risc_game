package RISC.Client.SHARE.game;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * Build different GameMap for different number of player
 */
public class GameMapFactory {

    /**
     * Create different game map for different number of player
     *
     * @param playerNum is the number of player
     * @return the created game map
     */
    public GameMap createMap(int playerNum, List<Integer> playerIDs) {
        GameMap gameMap = new GameMap();
        switch (playerNum) {
            case 2:
                addTerritoriesFor2Players(gameMap);
                initMap(gameMap, playerNum, playerIDs);
                return gameMap;
            case 3:
                addTerritoriesFor3Players(gameMap);
                initMap(gameMap, playerNum, playerIDs);
                return gameMap;
            case 4:
                addTerritoriesFor4Players(gameMap);
                initMap(gameMap, playerNum, playerIDs);
                return gameMap;
            case 5:
                addTerritoriesFor5Players(gameMap);
                initMap(gameMap, playerNum, playerIDs);
                return gameMap;
            default:
                return null;
        }
    }

    /**
     * Assign the territories for each player
     *
     * @param map       the map to assign
     * @param playerNum is the number of player
     */
    private void initMap(GameMap map, int playerNum, List<Integer> playerIDs) {
        int counter = 0;
        for (int playerID : playerIDs) {
            for (Territory territory : map.getTerritories()) {
                if (territory.getInitialGroup() == counter) {
                    territory.setOwner(playerID);
                }
            }
            counter++;
        }
    }

    /**
     * Create territories and assign neighbors for each territory
     *
     * @param map is the map to assign
     */
    private void addTerritoriesFor2Players(GameMap map) {
        LinkedHashSet<Territory> res = new LinkedHashSet<>();

        Territory Narnia = new Territory("Narnia", 0);
        Territory Midkemia = new Territory("Midkemia", 0);
        Territory Oz = new Territory("Oz", 0);
        Territory Gondor = new Territory("Gondor", 0);
        Territory Elantris = new Territory("Elantris", 1);
        Territory Roshar = new Territory("Roshar", 1);
        Territory Scadrial = new Territory("Scadrial", 1);
        Territory Mordor = new Territory("Mordor", 1);

        Narnia.addNeighbors(Elantris, Midkemia);
        Midkemia.addNeighbors(Narnia, Elantris, Scadrial, Oz);
        Oz.addNeighbors(Midkemia, Scadrial, Mordor, Gondor);
        Elantris.addNeighbors(Roshar, Scadrial, Midkemia, Narnia);

        Roshar.addNeighbors(Scadrial, Elantris);
        Scadrial.addNeighbors(Elantris, Roshar, Mordor, Oz, Midkemia, Elantris);
        Gondor.addNeighbors(Oz, Mordor);
        Mordor.addNeighbors(Gondor, Oz, Scadrial);

        Collections.addAll(res, Narnia, Midkemia, Oz, Gondor, Elantris, Roshar, Scadrial, Mordor);

        map.setTerritories(res);
    }

    /**
     * Create territories and assign neighbors for each territory
     *
     * @param map is the map to assign
     */
    private void addTerritoriesFor3Players(GameMap map) {
        LinkedHashSet<Territory> res = new LinkedHashSet<>();

        Territory Narnia = new Territory("Narnia", 0);
        Territory Midkemia = new Territory("Midkemia", 0);
        Territory Oz = new Territory("Oz", 0);
        Territory Gondor = new Territory("Gondor", 1);
        Territory Elantris = new Territory("Elantris", 2);
        Territory Roshar = new Territory("Roshar", 2);
        Territory Scadrial = new Territory("Scadrial", 2);
        Territory Mordor = new Territory("Mordor", 1);
        Territory Hogwarts = new Territory("Hogwarts", 1);

        Narnia.addNeighbors(Elantris, Midkemia);
        Midkemia.addNeighbors(Narnia, Elantris, Scadrial, Oz);
        Oz.addNeighbors(Midkemia, Scadrial, Mordor, Gondor);

        Elantris.addNeighbors(Roshar, Scadrial, Midkemia, Narnia);
        Roshar.addNeighbors(Hogwarts, Scadrial, Elantris);
        Scadrial.addNeighbors(Elantris, Roshar, Hogwarts, Mordor, Oz, Midkemia, Elantris);

        Gondor.addNeighbors(Oz, Mordor);
        Mordor.addNeighbors(Hogwarts, Gondor, Oz, Scadrial);
        Hogwarts.addNeighbors(Mordor, Scadrial, Roshar);

        Collections.addAll(res, Narnia, Midkemia, Oz, Gondor, Elantris, Roshar, Scadrial, Mordor, Hogwarts);
        map.setTerritories(res);
    }

    /**
     * Create territories and assign neighbors for each territory
     *
     * @param map is the map to assign
     */
    private void addTerritoriesFor4Players(GameMap map) {
        LinkedHashSet<Territory> res = new LinkedHashSet<>();

        Territory Narnia = new Territory("Narnia", 0);
        Territory Midkemia = new Territory("Midkemia", 0);
        Territory Oz = new Territory("Oz", 1);
        Territory Gondor = new Territory("Gondor", 1);
        Territory Elantris = new Territory("Elantris", 2);
        Territory Roshar = new Territory("Roshar", 2);
        Territory Scadrial = new Territory("Scadrial", 3);
        Territory Mordor = new Territory("Mordor", 3);

        Narnia.addNeighbors(Elantris, Midkemia);
        Midkemia.addNeighbors(Narnia, Elantris, Scadrial, Oz);

        Oz.addNeighbors(Midkemia, Scadrial, Mordor, Gondor);
        Elantris.addNeighbors(Roshar, Scadrial, Midkemia, Narnia);

        Roshar.addNeighbors(Scadrial, Elantris);
        Scadrial.addNeighbors(Elantris, Roshar, Mordor, Oz, Midkemia, Elantris);

        Gondor.addNeighbors(Oz, Mordor);
        Mordor.addNeighbors(Gondor, Oz, Scadrial);

        Collections.addAll(res, Narnia, Midkemia, Oz, Gondor, Elantris, Roshar, Scadrial, Mordor);

        map.setTerritories(res);
    }

    /**
     * Create territories and assign neighbors for each territory
     *
     * @param map is the map to assign
     */
    private void addTerritoriesFor5Players(GameMap map) {
        LinkedHashSet<Territory> res = new LinkedHashSet<>();

        Territory Narnia = new Territory("Narnia", 0);
        Territory Midkemia = new Territory("Midkemia", 0);
        Territory Oz = new Territory("Oz", 1);
        Territory Gondor = new Territory("Gondor", 1);
        Territory Elantris = new Territory("Elantris", 2);
        Territory Roshar = new Territory("Roshar", 2);
        Territory Scadrial = new Territory("Scadrial", 3);
        Territory Mordor = new Territory("Mordor", 3);
        Territory Hogwarts = new Territory("Hogwarts", 4);
        Territory Bogo = new Territory("Bogo", 4);

        Narnia.addNeighbors(Elantris, Midkemia);
        Midkemia.addNeighbors(Narnia, Elantris, Scadrial, Oz);

        Oz.addNeighbors(Midkemia, Scadrial, Mordor, Gondor);
        Elantris.addNeighbors(Roshar, Scadrial, Midkemia, Narnia);

        Roshar.addNeighbors(Hogwarts, Scadrial, Elantris);
        Scadrial.addNeighbors(Elantris, Roshar, Hogwarts, Mordor, Oz, Midkemia, Elantris);

        Gondor.addNeighbors(Oz, Mordor);
        Mordor.addNeighbors(Hogwarts, Gondor, Oz, Scadrial);

        Hogwarts.addNeighbors(Mordor, Scadrial, Roshar);
        Bogo.addNeighbors(Hogwarts, Mordor, Oz);
        Collections.addAll(res, Narnia, Midkemia, Oz, Gondor, Elantris, Roshar, Scadrial, Mordor, Hogwarts, Bogo);

        map.setTerritories(res);
    }
}
