package RISC.Client.SHARE.game;

import java.util.HashSet;

public interface AbstractMap {
    /**
     * Get all the territories belongs to the player
     *
     * @param playerID is the id of player to get
     * @return the territories
     */
    HashSet<Territory> getTerritoriesForPlayer(int playerID);

    /**
     * Get the territory of a name
     *
     * @param name territory name
     * @return the territory
     */
    Territory getTerritoryByName(String name);

    /**
     * Get all territories
     *
     * @return all territories
     */
    HashSet<Territory> getTerritories();

}
