package RISC.Client.SHARE.rule;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class EnoughFoodToAttackCheckerTest {
    @Test
    public void test_enoughFoodToAttack() {
        EnoughFoodToAttackChecker enoughFoodToAttackChecker = new EnoughFoodToAttackChecker(null);
        assertThrows(NullPointerException.class, ()->enoughFoodToAttackChecker.checkMyRule(null, null, null));
    }

}