package RISC.Client.SHARE.game;

import java.io.Serializable;
import java.util.*;

// Maintainer: Ruitong Su
public class Territory implements AbstractTerritory, Serializable {
    private final String name;
    private final int initialGroup;
    private int ownerID;
    private HashSet<Territory> neighbors;
    private final int size;
    private final int foodProduction;
    private final int techProduction;

    private int unitsNum;   // EVAL-1
    /*EVAL-2*/
    private List<Integer> unitList;

    public int remainingCloaking;
    private Set<SpyUnit> allSpys;

    /**
     * Construct a territory
     *
     * @param name         is the name of the territory
     * @param initialGroup is the initial group of territory
     */
    public Territory(String name, int initialGroup) {
        this.name = name;
        this.initialGroup = initialGroup;
        this.unitList = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0));
        this.size = 2;
        this.foodProduction = 50;
        this.techProduction = 50;
        this.neighbors = new HashSet<>();
        this.remainingCloaking = 0;
        this.allSpys = new HashSet<>();
    }

    public boolean isVisible(int playerID) {
        if (playerID == ownerID) { //owner can see it
            return true;
        }

        for (SpyUnit spyUnit : allSpys) {
            if (spyUnit.getOwnerID() == playerID) { //spy can see it
                return true;
            }
        }

        if (remainingCloaking > 0) { //if remain cloak, nobody else can see it
            return false;
        }

        HashSet<Territory> neighbors = getNeighbors();
        for (Territory neighbor : neighbors) { //adjacent player can see it
            if (neighbor.getOwnerID() == playerID) {
                return true;
            }
        }
        return false;
    }

    public void removeSpy(SpyUnit spy) {
        this.allSpys.remove(spy);
    }

    public void addSpy(SpyUnit spy) {
        this.allSpys.add(spy);
    }

    /**
     * Construct a territory
     *
     * @param name           is the name of the territory
     * @param initialGroup   is the initial group of territory
     * @param size           is the size of the territory
     * @param foodProduction is the food production rate of the territory
     * @param techProduction is the tech production rate of the territory
     */
    public Territory(String name, int initialGroup, int size, int foodProduction, int techProduction) {
        this.name = name;
        this.initialGroup = initialGroup;
        this.unitList = new ArrayList<>(Arrays.asList(0, 0, 0, 0, 0, 0, 0));
        this.size = size;
        this.foodProduction = foodProduction;
        this.techProduction = techProduction;
        neighbors = new HashSet<>();
    }

    public void updateUnitList(int num, int level) {
        unitList.set(level, unitList.get(level) + num);
    }

    public int getUnitNumByLevel(int level) {
        return unitList.get(level);
    }

    public int getTotalNumOfUnits() {
        return unitList.stream().mapToInt(Integer::intValue).sum();
    }

    public Set<Territory> getOwnedNeighbors() {
        Set<Territory> result = new HashSet<>();
        for (Territory territory : neighbors) {
            if (territory.getOwnerID() == this.ownerID) {
                result.add(territory);
            }
        }
        return result;
    }

    /**
     * Add neighbors to territory
     *
     * @param collection is the neighbors to add
     */
    @Override
    public void addNeighbors(Territory... collection) {
        neighbors.addAll(Arrays.asList(collection));
    }

    /**
     * Add neighbor to territory
     *
     * @param territory is the neighbor to add
     */
    @Override
    public void addNeighbor(Territory territory) {
        neighbors.add(territory);
    }

    /**
     * Increase the number of units for the territory
     *
     * @param unitsToIncrease is the number units to increase
     */

    public void increaseUnitsNum(List<Integer> unitsToIncrease) {
        assert unitsToIncrease.size() == 7;
        for (int i = 0; i < unitsToIncrease.size(); i++) {
            this.unitList.set(i, this.unitList.get(i) + unitsToIncrease.get(i));
        }
    }

    public void increaseUnitsNum(int level, int num) {
        this.unitList.set(level, this.unitList.get(level) + num);
    }

    public void increaseBasicUnitByOne(int unitsToIncrease) {
        this.unitList.set(0, this.unitList.get(0) + 1);
    }

    /**
     * Decrease the number of units for the territory
     *
     * @param unitsToDecrease is the number units to decrease
     */
//    public void decreaseUnitsNum(int num) {
//        this.unitsNum -= num;
//    }
    public void decreaseUnitsNum(List<Integer> unitsToDecrease) {
        assert unitsToDecrease.size() == 7;
        for (int i = 0; i < unitsToDecrease.size(); i++) {
            this.unitList.set(i, this.unitList.get(i) - unitsToDecrease.get(i));
        }
    }

    public void decreaseUnitsNum(int level, int num) {
        this.unitList.set(level, this.unitList.get(level) - num);
    }


    /*Setter*/
    @Override
    public void setOwner(int ownerID) {
        this.ownerID = ownerID;
    }

    /*Getters*/
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getOwnerID() {
        return ownerID;
    }

    @Override
    public int getUnitsNum() {
        return unitsNum;
    }

    @Override
    public void setUnitsNum(int unitsNum) {
        this.unitsNum = unitsNum;
    }

    public int getInitialGroup() {
        return initialGroup;
    }

    public int getSize() {
        return size;
    }

    public List<Integer> getUnitList() {
        return unitList;
    }


    public int getFoodProduction() {
        return foodProduction;
    }

    public int getTechProduction() {
        return techProduction;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(unitList).append(" units in ").append(name).append(" (next to: ");
        for (Territory territory : neighbors) {
            sb.append(territory.getName()).append(", ");
        }
        if (!neighbors.isEmpty()) {
            sb.deleteCharAt(sb.lastIndexOf(","));
            sb.deleteCharAt(sb.lastIndexOf(" "));
        }
        sb.append(")");
        return sb.toString();
    }

    public HashSet<Territory> getNeighbors() {
        return neighbors;
    }

    public SpyUnit getCanMoveSpy() {
        for(SpyUnit spyUnit:allSpys){
            if(spyUnit.isAvailableToMove()){
                return spyUnit;
            }
        }
        return null;
    }
}
