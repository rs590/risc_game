package RISC;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ServerTest {
    @Test
    public void test_server() {
        assertDoesNotThrow(() -> Server.getInstance());
    }
    @Test
    public void test_Singleton() {
        Server server = null;
        assertEquals(server, Server.getInstance());
        assertDoesNotThrow(() -> Server.getInstance());
    }
}