package RISC.Client.SHARE.rule;


import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;

import java.util.List;

/**
 * Check is the territory name is valid
 */
public class ValidTerritoryNameChecker extends RuleChecker {
    public ValidTerritoryNameChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        List<String> from = action.from;
        List<String> to = action.to;

        for (String s : from) {
            if (gameMap.getTerritoryByName(s) == null) {
                return "Territory name not found: " + s;
            }
        }

        for (String s : to) {
            if (gameMap.getTerritoryByName(s) == null) {
                return "Territory name not found: " + s;
            }
        }
        return null;
    }
}
