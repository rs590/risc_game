package RISC.Client.SHARE.result;

import RISC.Client.SHARE.game.Territory;

import java.io.Serializable;

public class MoveResult implements Serializable {
    public Territory from;
    public Territory to;
    public int foodcost;
    public int num;

    public MoveResult(Territory from, Territory to, int foodcost, int num) {
        this.from = from;
        this.to = to;
        this.foodcost = foodcost;
        this.num = num;
    }
}
