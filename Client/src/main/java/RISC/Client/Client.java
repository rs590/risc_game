package RISC.Client;

import RISC.Client.SHARE.action.ChooseGameRoomInfo;
import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.GameInfo;
import RISC.Client.SHARE.utility.SerializeUtility;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * The Client of the RISK game.
 * Each client represent a player.
 * Text information about the game will be presented by player.
 * And player needs to send his action to server to play game with others online.
 * PORT: port number default '8899'.
 * HOST: host name default 'localhost'.
 * socket: the socket with the sever host.
 * utility: the serializable utility to serialize message to server.
 * input: input stream from socket.
 * output: output stream from socket.
 * gameInfo: the game info send by the server.
 * local_reader: the client local input reader.
 * textView: the text view controller for player.
 * message: the message to send to server.*/
public class Client {
    private final int PORT = 8899;
    private final String HOST = "localhost";
    //private final String HOST = "dku-vcm-2032.vm.duke.edu";

    public Socket socket;
    public SerializeUtility utility;
    private InputStream input;
    private OutputStream output;
    private GameInfo gameInfo;
    private BufferedReader local_reader;
    private TextView textView;
    private Message message;

    /**
     * construction of Client
     * Use initClient method to init client.*/
    public Client() throws IOException, ClassNotFoundException {
        this.utility = new SerializeUtility();
        this.local_reader = new BufferedReader(new InputStreamReader(System.in));
        initClient();
        utility.recvGameInfo(input);
    }

    /**
     * Used to create connection with server by socket.*/
    private void initClient() {
        try {
            InetAddress ip = InetAddress.getByName(HOST);
            this.socket = new Socket(ip, PORT);
            socket.setKeepAlive(true);
            this.input = this.socket.getInputStream();
            this.output = this.socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to do init operation for the game.
     * 1. print basic info of current player.
     * 2. print info to let player set units to his territories.*/
    public GameInfo gameInit() throws IOException {

        System.out.println("<<Welcome to RISC game! Waiting other players to join..>>");

        input = socket.getInputStream();
        gameInfo = utility.recvGameInfo(input);

        System.out.println("<<You are connected as player No." + gameInfo.player.getId() + ">>");
        return this.gameInfo;
    }

    /**
     * Used to play one turn for player.
     * 1. receive game info from the server.
     * 2. print the game info and guide player to input his actions.
     * 3. serialize actions into message.
     * 4. send message to the server and wait for the next turn.
     */
//    public void play() throws IOException {
//        while (true) {
//            gameInfo = utility.recvGameInfo(input); //blocking
//            local_reader = new BufferedReader(new InputStreamReader(System.in));
//            boolean is_end = textView.updatGameInfo(gameInfo);
//            if (is_end) { //game is end
//                return;
//            }
//            playOneRound();
//        }
//    }

    /**
     * Used to exchange the message with Sever*/
    public GameInfo changeMessage(Message message) throws IOException {
        utility.sendMessage(message, output);
        gameInfo = utility.recvGameInfo(input); //blocking
        return gameInfo;
    }

    /**
     * Text based play one round option*/
//    private void playOneRound() throws IOException {
//        this.message = new Message();
//        if (!textView.is_watcher) {
//            textView.showText();
//            message.updateActionList(textView.attackAction, textView.moveAction, textView.upgradeUnitsAction, textView.upgradeTechLevelAction, textView.upgradeSpyAction, textView.spyMoveAction, textView.cloackingAction);
//            utility.sendMessage(message, output);
//            System.out.println("<<Your actions are committed. Please wait for other players...>>");
//        } else {
//            textView.printMapInfo();
//            message.updateActionList(textView.attackAction, textView.moveAction, textView.upgradeUnitsAction, textView.upgradeTechLevelAction, textView.upgradeSpyAction, textView.spyMoveAction, textView.cloackingAction);
//            utility.sendMessage(message, output);
//        }
//    }

    /**
     * User login option
     * @param message the login message send to Sever
     * @return if login success or not*/
    public boolean userLogin(Message message) throws IOException {
        utility.sendMessage(message, output);
        GameInfo loginInfo = utility.recvGameInfo(input);
        if (loginInfo.loginSuccess) {
            utility.recvChooseGameRoomInfo(input);
            return true;
        }
        return false;
    }

    /**
     * User select game option
     * @param message the game choose message send to Sever
     * @return if choose game success or not*/
    public boolean userSelectGame(Message message) throws IOException {
        utility.sendMessage(message, output);
        ChooseGameRoomInfo chooseGameRoomInfo = utility.recvChooseGameRoomInfo(input);
        if (chooseGameRoomInfo.isChooseRoomSuccess()) {
            return true;
        }
        System.out.println(chooseGameRoomInfo);
        System.out.println("choose game succeed");
        return false;
    }

    /**
     * Used to disconnect from server and print the end info.
     */
    public void closeSession() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The main function of the Client.*/
    public static Client getInstance() throws IOException, ClassNotFoundException {
        Client client = new Client();
        return client;
    }
}

