package RISC;


import RISC.Client.SHARE.action.Message;
import RISC.Client.SHARE.game.Player;

import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class MessageReceiver {
    private volatile Map<Integer, Message> messages; //message queue
    private Map<Player, MessageHandler> playerToHandler;
    private CyclicBarrier actionBarrier; //barrier for message synchronization

    public MessageReceiver(int numPlayer) {
        this.messages = new HashMap<>();
        this.playerToHandler = new HashMap<>();
        this.actionBarrier = new CyclicBarrier(numPlayer + 1);
    }

    /**
     * This method is blocking.
     *
     * @return Clone of all messages from all players
     */
    public Map<Integer, Message> getAllMessages() {
        update();
        this.messages.clear(); //clear all messages
        waitMessageFromAllPlayers();
        System.out.println("get all messgaes");
        Map<Integer, Message> temp = new HashMap<>(messages);
        return temp;
    }

    /**
     * Start a new message handler
     *
     * @param player player for listen
     * @param socket socket for communication
     */
    public void addHandler(Player player, Socket socket) {
        MessageHandler newHandler = new MessageHandler(player, socket, this, actionBarrier);
        this.playerToHandler.put(player, newHandler);
        newHandler.start();
    }

    public void removeHandler(Player player) {
        this.playerToHandler.get(player).interrupt(); //stop the handler;
        this.playerToHandler.put(player, null); //set handler to null
    }

    /**
     * Handler invokes this method to add new message to the message queue
     */
    public synchronized void addMessage(int playerID, Message message) {
        this.messages.put(playerID, message);
    }

    /**
     * Wait messages for all players, blocking method
     */
    private void waitMessageFromAllPlayers() {
        try {
            actionBarrier.await(); //wait all player sending their messages
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    /**
     * Update handler map and reset barrier
     */
    private void update() {
        //set dead handler to null
        for (Map.Entry<Player, MessageHandler> entry : playerToHandler.entrySet()) {
            Player player = entry.getKey();
            MessageHandler handler = entry.getValue();
            if (!handler.isAlive()) {
                playerToHandler.put(player, null);
            }
        }
        //update barrier
        int num = 1;
        for (MessageHandler handler : playerToHandler.values()) {
            if (handler != null) {
                num++;
            }
        }

        this.actionBarrier = new CyclicBarrier(num); //when a player lose and exit, need to reset the barrier
        sendNewBarrier(this.actionBarrier);
    }

    /**
     * Send new barrier to all handlers
     *
     * @param barrier new barrier to send
     */
    private void sendNewBarrier(CyclicBarrier barrier) {
        for (MessageHandler handler : playerToHandler.values()) {
            if (handler != null) {
                handler.actionBarrier = barrier;
            }
        }
    }

    /**
     * Close all handler threads
     */
    public void close() {
        for (MessageHandler handler : playerToHandler.values()) {
            if (handler != null) {
                handler.interrupt();
            }
        }
    }

    /**
     * Is the player's message received or not
     *
     * @param player A player
     * @return Received or not
     */
    public boolean isReceived(Player player) {
        if (!this.messages.containsKey(player.getId())) {
            return false;
        }
        return this.messages.get(player.getId()) != null;
    }
}
