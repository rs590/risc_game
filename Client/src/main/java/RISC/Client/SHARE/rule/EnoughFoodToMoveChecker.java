package RISC.Client.SHARE.rule;


import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;
import RISC.Client.SHARE.game.Territory;

public class EnoughFoodToMoveChecker extends RuleChecker {

    public EnoughFoodToMoveChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    /**
     * Check the action is valid or not
     *
     * @param gameMap game map
     * @param action  action
     * @return valid or not
     */
    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        for (int i = 0; i < action.from.size(); i++) {
            String from = action.from.get(i);
            String to = action.to.get(i);
            Territory source = gameMap.getTerritoryByName(from);
            Territory dest = gameMap.getTerritoryByName(to);
            int minCost = gameMap.minimalCostPath(source, dest);
            int totalFoodResource = player.getTotalFoodResource();
            boolean isValidMove = minCost <= totalFoodResource;
            if (!isValidMove) {
                return "err: " + "move from " + from + " to " + to + " needs " + minCost + " foods, but just got " + totalFoodResource;
            }
        }
        return null;
    }
}
