package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UpgradeTechLevelActionTest {
    @Test
    public void test_upgradeTechLevel() {
        UpgradeTechLevelAction upgradeTechLevelAction = new UpgradeTechLevelAction();
        assertThrows(NullPointerException.class, ()->upgradeTechLevelAction.execute(null, null, null));

    }

}