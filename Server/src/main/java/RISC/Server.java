package RISC;

import RISC.Client.SHARE.game.Player;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class is used for supporting all games and listen for connection
 *
 * @author cirun zhang
 */
public class Server {
    public static Server server;  //singleton server instance
    private ServerSocket serverSocket;  //listen socket
    private volatile Map<Integer, Player> allGlobalPlayers;
    private volatile Map<Integer, Game> allGlobalGames;

    private UserDB userDB;
    static {
        server = new Server(8899);
    }

    /**
     * Singleton constructor
     *
     * @param port Listen port
     */
    private Server(int port) {
        System.out.println("server started");
        this.allGlobalPlayers = new ConcurrentHashMap<>();
        this.allGlobalGames = new ConcurrentHashMap<>();
        this.userDB = new UserDB();
        initSever(port);
        createAllGames();
        startAllGames();
        listen();
    }

    /**
     * Listen for socket
     */
    private void listen() {
        Socket socket;
        while (true) {
            try {
                socket = serverSocket.accept();
                socket.setKeepAlive(true);
                System.out.println("got a connection!");
                new PlayerHandler(socket, allGlobalPlayers, allGlobalGames, userDB).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create 4 game rooms
     */
    private void createAllGames() {
        allGlobalGames.put(0, new Game(2, 0, "2 Players' Room"));
        allGlobalGames.put(1, new Game(3, 1, "3 Players' Room"));
        allGlobalGames.put(2, new Game(4, 2, "4 Players' Room"));
        allGlobalGames.put(3, new Game(5, 3, "5 Players' Room"));
    }

    private void startAllGames() {
        for (Game game : allGlobalGames.values()) {
            game.start(); //start game thread
        }
    }

    /**
     * Get singleton instance
     *
     * @return Singleton instance
     */
    public static Server getInstance() {
        return server;
    }

    /**
     * Start the server, bind the port, accept all players' connections
     *
     * @param port The port to listen
     */
    private void initSever(int port) {
        try {
            serverSocket = new ServerSocket(port); //bind port
            System.out.println("server running at: " + serverSocket.getInetAddress().getHostName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void close() {
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
