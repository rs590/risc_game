//import RISC.Client.TextView;
//import RISC.Client.SHARE.action.SetUnitAction;
//import RISC.Client.SHARE.game.*;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.parallel.ResourceAccessMode;
//import org.junit.jupiter.api.parallel.ResourceLock;
//import org.junit.jupiter.api.parallel.Resources;
//
//import java.io.*;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class TextViewTest {
//    GameInfo gameInfo;
//    BufferedReader local_reader;
//   public void init(){
//       local_reader = new BufferedReader(new InputStreamReader(System.in));
//       GameMap map = new GameMapFactory().createMap(3);
//       Player player0 = new Player(0,map.getTerritoriesForPlayer(0));
//       Player player1 = new Player(1,map.getTerritoriesForPlayer(1));
//       Player player2 = new Player(2,map.getTerritoriesForPlayer(2));
//       List<Player> playerList = new ArrayList<>();
//       playerList.add(player0);
//       playerList.add(player1);
//       playerList.add(player2);
//       boolean isGameEnd = false;
//       gameInfo = new GameInfo(map,player0,playerList,isGameEnd,3,30);
//   }
//
//   @Test
//   public void TestUpdatGameInfo() throws IOException {
//       init();
//       boolean isGameEnd = true;
//       GameInfo gameInfo1 = new GameInfo(gameInfo.map,gameInfo.player,gameInfo.playerList,isGameEnd,3,30);
//       Player player = new Player(0,null);
//       player.setLost(true);
//       GameInfo gameInfo2 = new GameInfo(gameInfo.map,player,gameInfo.playerList,false,3,30);
//       Player player1 = new Player(0,null);
//       player1.setWin(true);
//       GameInfo gameInfo3 = new GameInfo(gameInfo.map,player1,gameInfo.playerList,false,3,30);
//       StringReader sr = new StringReader("6\nn");
//       BufferedReader bf = new BufferedReader(sr);
//       TextView textView = new TextView(gameInfo,bf);
//       Assertions.assertTrue(textView.updatGameInfo(gameInfo1));
//       Assertions.assertFalse(textView.updatGameInfo(gameInfo2));
//       Assertions.assertFalse(textView.updatGameInfo(gameInfo));
//       Assertions.assertTrue(textView.updatGameInfo(gameInfo3));
//   }
//
//   @Test
//    public void TestCreateSetUnitAction() throws IOException {
//       init();
//       StringReader sr = new StringReader("r\n3\n70\nr\n70\n10\n10\n");
//       BufferedReader bf = new BufferedReader(sr);
//       List<String> list1 = new ArrayList<>();
//       for(Territory territory:gameInfo.map.getTerritoriesForPlayer(gameInfo.player.getId())){
//           list1.add(territory.getName());
//       }
//       List<Integer> list2 = new ArrayList<>();
//       list2.add(3);
//       list2.add(10);
//       list2.add(10);
//       SetUnitAction setUnitAction = new SetUnitAction(list1,list2);
//       TextView textView = new TextView(gameInfo,bf);
//       assertEquals(setUnitAction,textView.createSetUnitAction());
//   }
//
//   @Test
//   @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
//    public void TestshowText() throws IOException {
//       init();
//       ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//       PrintStream out = new PrintStream(bytes, true);
//       InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
//       assertNotNull(input);
//       InputStream expectedStream =
//               getClass().getClassLoader().getResourceAsStream("output.txt");
//       assertNotNull(expectedStream);
//       BufferedReader bf = new BufferedReader(new InputStreamReader(input));
//       for(Territory territory:gameInfo.player.getTerritoryList()){
//           territory.setUnitsNum(10);
//           System.out.println(territory.getName());
//       }
//       TextView textView = new TextView(gameInfo,bf);
//       textView.showText();
//       StringBuilder sb = new StringBuilder();
//       String line;
//       BufferedReader br = new BufferedReader(new InputStreamReader(expectedStream));
//       while ((line = br.readLine()) != null) {
//           sb.append(line).append("\n");
//       }
//       String expected = sb.toString();
//       String actual = bytes.toString();
//       assertFalse(expected.equals(actual));
//   }
//
//   @Test
//    public void TestPrintEndInfo() throws IOException {
//       init();
//       boolean isGameEnd = true;
//       GameInfo gameInfo1 = new GameInfo(gameInfo.map,gameInfo.player,gameInfo.playerList,isGameEnd,3,30);
//       Player player = new Player(0,null);
//       player.setWin(true);
//       GameInfo gameInfo2 = new GameInfo(gameInfo.map,player,gameInfo.playerList,false,3,30);
//       Player player1 = new Player(0,null);
//       player1.setWin(true);
//       GameInfo gameInfo3 = new GameInfo(gameInfo.map,player1,gameInfo.playerList,true,3,30);
//       TextView textView = new TextView(gameInfo,local_reader);
//       textView.printEndInfo();
//       textView.updatGameInfo(gameInfo1);
//       textView.printEndInfo();
//       TextView textView1 = new TextView(gameInfo3,local_reader);
//       textView1.printEndInfo();
//   }
//}
