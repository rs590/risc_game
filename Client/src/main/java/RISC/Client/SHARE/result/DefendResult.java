package RISC.Client.SHARE.result;

import RISC.Client.SHARE.game.Territory;

import java.io.Serializable;

public class DefendResult implements Serializable {
    public Territory to;
    public int attacker;
    public int level;
    public int num;
    public int num_left;
    public boolean result;

    public DefendResult(Territory to, int attacker, int level, int num, int num_left, boolean result) {
        this.to = to;
        this.attacker = attacker;
        this.level = level;
        this.num = num;
        this.num_left = num_left;
        this.result = result;
    }
}
