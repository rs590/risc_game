package RISC.Client.SHARE.action;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class SpyMoveActionTest {
    @Test
    public void test_spyMove() {
        SpyMoveAction spyMoveAction = new SpyMoveAction();
        assertDoesNotThrow(()->spyMoveAction.execute(null, null, null));
        spyMoveAction.addOneAction(null, null);
    }
}