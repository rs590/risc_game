package RISC.Client.SHARE.rule;

import RISC.Client.SHARE.action.Action;
import RISC.Client.SHARE.game.GameMap;
import RISC.Client.SHARE.game.Player;

import java.util.List;

public class EnoughFoodToAttackChecker extends RuleChecker {

    public EnoughFoodToAttackChecker(RuleChecker nextRule) {
        super(nextRule);
    }

    @Override
    protected String checkMyRule(GameMap gameMap, Player player, Action action) {
        int food = player.getTotalFoodResource();
        for (int i = 0; i < action.from.size(); i++) {
            List<Integer> unitList = action.units.get(i);
            int foodToConsume = 0;
            for (int unit : unitList) {
                foodToConsume += unit;
            }
            food -= foodToConsume;
            if (food < 0) {
                return "err: no enough food to attack";
            }
        }
        return null;
    }
}
