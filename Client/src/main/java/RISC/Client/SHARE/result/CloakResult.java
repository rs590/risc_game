package RISC.Client.SHARE.result;

import RISC.Client.SHARE.game.Territory;

import java.io.Serializable;

public class CloakResult implements Serializable {
    public Territory where;

    public CloakResult(Territory where) {
        this.where = where;
    }
}
