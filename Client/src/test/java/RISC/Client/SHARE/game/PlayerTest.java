package RISC.Client.SHARE.game;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PlayerTest {

    static GameMapFactory factory = new GameMapFactory();

    ArrayList<Integer> createPlayerList(int playerNum) {
        ArrayList<Integer> playerIDs = new ArrayList<>();
        playerIDs.add(0);
        playerIDs.add(1);
        switch (playerNum) {
            case 5:
                playerIDs.add(4);
            case 4:
                playerIDs.add(3);
            case 3:
                playerIDs.add(2);
                break;
        }
        return playerIDs;
    }

    @Test
    public void test_player() {
        GameMap map = factory.createMap(2, createPlayerList(2));
        Player test = new Player(88, 88);
        Player player_0 = new Player(0, map.getTerritoriesForPlayer(0));
        Player player_1 = new Player(1, map.getTerritoriesForPlayer(1));

        player_0.updateVisited(map);
        assertNotNull(player_0.getVisited());
        assertEquals(0, player_0.getSpyByTerritory(map.getTerritoryByName("Narnia")));
        player_0.addSpy(1, map.getTerritoryByName("Narnia"));
        assertEquals(1, player_0.getSpyByTerritory(map.getTerritoryByName("Narnia")));
        player_0.setAllSpyAvaliableToMove();
        assertNotNull(player_0.getSpyUnits());
        player_0.clearAllResults();
        player_0.addResourcesFromTerritories();
    }
}