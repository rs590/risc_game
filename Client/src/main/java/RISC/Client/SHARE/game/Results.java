package RISC.Client.SHARE.game;

import RISC.Client.SHARE.result.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Results implements Serializable {
    public List<MoveResult> list_move;
    public List<AttackResult> list_attack;
    public List<DefendResult> list_defend;
    public List<UpgradeResult> list_upgrade;
    public List<CloakResult> list_cloak;

    public Results() {
        list_move = new ArrayList<>();
        list_attack = new ArrayList<>();
        list_defend = new ArrayList<>();
        list_upgrade = new ArrayList<>();
        list_cloak = new ArrayList<>();
    }

    public boolean isEmpty(){
        return list_attack.isEmpty()&&list_defend.isEmpty()&&list_move.isEmpty()&&list_upgrade.isEmpty()&&
                list_cloak.isEmpty();
    }

    public void clearAllResult(){
        list_move.clear();
        list_upgrade.clear();
        list_defend.clear();
        list_attack.clear();
        list_cloak.clear();
    }

    public void addAResult(MoveResult moveResult){
        list_move.add(moveResult);
    }

    public void addAResult(AttackResult attackResult){
        list_attack.add(attackResult);
    }

    public void addAResult(DefendResult defendResult){
        list_defend.add(defendResult);
    }

    public void addAResult(UpgradeResult upgradeResult){
        list_upgrade.add(upgradeResult);
    }

    public void addAResult(CloakResult cloakResult){
        list_cloak.add(cloakResult);
    }
}
